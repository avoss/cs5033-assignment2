## About Sha.Re

The Sha.Re software product line was developed in the School of Computer Science at St Andrews University as a teaching tool. While we are doing our best to produce high quality code, at this point we cannot guarantee that this code if fit to be used in actual implementation projects. Note the comments on limitations below. 

## Sha.Re Software Product Pipeline Rules

The Sha.Re software product pipeline is highly configurable. None of the technologies involved are strictly required as most code consists of Plain Old Java Objects (POJOs). So, in the simplest case, you can develop a product as a simple desktop application with very few runtime dependencies. You can choose your own approaches to persisting your data, to building a RESTful API and to implementing modularity and service-orientation. 

Here are some guidelines that will hopefully help you write code that preserves the desirable properties of the Product Line: 

* If it’s not in the interface, don’t use it. Using features of a particular implementation is brittle. If you feel that some functionality should be available through an interface then please contact the author or [submit an issue](https://bitbucket.org/avoss/cs5033-assignment2/issues?status=new&status=open).  
* To ensure that the source code produced is easily readable and can be maintained by a community of people, use Google's coding standards. The automated builds run [Checkstyle](http://checkstyle.sourceforge.net/) and [Findbugs](http://findbugs.sourceforge.net/) to help ensure code quality. You can run these with `gradle check` and find the outputs in `build/

### Modifying Existing Code vs. New Project

As you have the source code for the software product line it is perfectly possible to create an implementation as a clone of the given source. This has the advantage that a lot of the supporting files such as `build.gradle` and configuration files are already in place. On the other hand, it will be easier to maintain your code if you create it as a separate project. If you decide to do this then you need to copy configuration files for Hibernate, the Bitronix transaction manager and Log4j2 to your project. They can be found in `src/main/resources` and `config`. 

### Object-Relational Mapping

The implementation of the model uses [Hibernate](http://hibernate.org/) to map Java objects to tables in a relational database management system. When implementing a new product, you may need to add to the underlying data model. This can be achieved by implementing new classes and adding them to the model or by inheriting from existing model classes if all that is required is to add some properties. In either case, you need to tell Hibernate about your new (child) classes by adding them to the `persistence.xml` file (can be found in `src/main/resource`).  

Another aspect of the object-relational mapping is the configuration of schema generation. This is set to `drop-and-create` in `persistence.xml`, meaning that Hibernate will try to drop all tables before re-creating them. This is useful for testing but in production needs to be changed to `none` so that Hibernate does not touch the database schema and data is retained.

### Database Configuration

The connection parameters for the relational database management system are configured in `config/datasources.properties` for production and `config/testdatasources.properties` respectively. To change the production properties to your [MariaDB instance on the host servers](https://systems.wiki.cs.st-andrews.ac.uk/index.php/Linux_Host_service#MySQL), use `config/mysqldatasources.properties`, replacing the username (including in the server name in the connection URL) and password. You will also need to activate the MySQL dialect in `persistence.xml`.	

### Dependency Injection

The system uses Jersey with [HK2 dependency injection](https://hk2.java.net/2.5.0-b03/) to wire together the different components. This happens at the time a request is made to the API. Jersey creates an instance of the API implementation for each request as well as instances of its dependencies: the model and the business rules. Note that the business rules also require a reference to the model. Unfortunately, even with its per-request semantics, Jersey/HK2 seem to be creating a new instance of the model each time it is injected. Therefore, the `BasicApi` class contains code to ensure that the business logic uses the same model instance.   

The configuration of the dependency injection happens in the `ShaReMain` class and you will need to replicate this in the main class you create for your project. The crucial lines of code are:

	rc.register(new AbstractBinder() {
      @Override
      protected void configure() {
        bindFactory(JpaModelFactory.class).to(Model.class);  
        bind(BasicLendingRules.class).to(LendingRules.class);
        bind(BasicRecallRules.class).to(RecallRules.class);
        bind(BasicReservationRules.class).to(ReservationRules.class);
      }
    });

You can see that we are using a factory for creating the `Model` instance since doing so requires work to establish the connection with the database by configuring Hibernate and Bitronix. The business rules classes can be instantiated directly by HK2 using their default constructor, so they can be bound to their respective interfaces directly.

### Direct Calls to Model

Note that the API classes make direct calls to the model for a number of operations. This is to avoid having to duplicate each method in the model class in the business logic. The downside is that it requires some discipline from the developers to check if a request needs to go through the business logic. Note that performance is not the issue here but maintainability of the code is.

## Limitations

Not addressed in the current version of the pipeline are the following requirements:

* Security 
* Internationalisation

There may be other omissions in the implementation. If they are not too onerous to fix I would be grateful if you could send me suggested patches, otherwise please file a bug report 

## License

Copyright 2016 Alexander Voss

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0).
  
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
limitations under the License.