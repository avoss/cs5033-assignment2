package re.sha.model.jpa;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

import re.sha.exceptions.ShareException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.Reservation;
import re.sha.model.User;

@Entity
@Table(name = "RESERVATIONS")
@Access(AccessType.FIELD)
public class JpaReservation implements Reservation {

  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;

  @ManyToOne
  @Expose
  private JpaCatalogueEntry catalogueEntry = null;

  @OneToOne
  @Expose
  private JpaHoldingItem holdingItem = null;

  @ManyToOne
  @Expose
  private JpaUser user = null;

  @Expose
  private LocalDateTime timestamp = null;
  
  /**
   * For the benefit of Hibernate only.
   */
  protected JpaReservation() {    
  }

  public JpaReservation(User user, CatalogueEntry entry) {
    this.user = JpaHelper.toJpaUser(user);
    this.catalogueEntry = JpaHelper.toJpaCatalogueEntry(entry);
    this.timestamp = LocalDateTime.now();
  }

  @Override
  public User getUser() {
    return this.user;
  }

  @Override
  public LocalDateTime getTimestamp() {
    return this.timestamp;
  }

  @Override
  public CatalogueEntry getCatalogueEntry() {
    return this.catalogueEntry;
  }

  @Override
  public void setCatalogueEntry(CatalogueEntry entry) {
    this.catalogueEntry = JpaHelper.toJpaCatalogueEntry(entry);
  }

  @Override
  public HoldingItem getHoldingItem() {
    return this.holdingItem;
  }

  @Override
  public void setHoldingItem(HoldingItem item) {
    this.holdingItem = JpaHelper.toJpaHoldingItem(item);
  }

  @Override
  public boolean isFulfilled() {
    return this.holdingItem != null;
  }

  @Override
  public HoldingItem getAvailableItem() throws ShareException {
    return this.holdingItem;
  }

  @Override
  public LocalDateTime getExpiryDateTime() throws ShareException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean isExpired() {
    // TODO Auto-generated method stub
    return false;
  }


}
