package re.sha.model.jpa;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

import re.sha.exceptions.ShareException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.Recall;
import re.sha.model.Reservation;
import re.sha.model.User;

@Entity
@Table(name = "HOLDING_ITEM")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) 
@DiscriminatorColumn(name = "ITEM_TYPE")
@DiscriminatorValue("I")
public class JpaHoldingItem implements HoldingItem {

  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;

  @Expose
  private long accessionNo = 0;

  @Expose
  private String location;

  @ManyToOne
  @NotNull
  @Expose
  private JpaCatalogueEntry catalogueEntry = null;

  @ManyToOne
  @NotNull
  @Expose
  private JpaUser user = null;

  @OneToOne
  private JpaLendingRecord lendingRecord;

  @OneToOne(mappedBy = "holdingItem")
  private JpaRecall recall = null;

  @OneToOne(mappedBy = "holdingItem")
  private JpaReservation reservation = null;

  @Override
  public long getId() {
    return this.id;
  }

  @Override
  public long getAccessionNumber() {
    return this.accessionNo;
  }

  @Override
  public CatalogueEntry getCatalogueEntry() {
    return this.catalogueEntry;
  }

  @Override
  public String getLocation() {
    return this.location;
  }

  @Override
  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public User getOwner() {
    return this.user;
  }

  @Override
  public void setOwner(User user) {
    this.user = JpaHelper.toJpaUser(user);    
  }

  @Override
  public boolean isAvailable() {
    if (this.isRecalled())
      return false;
    return (this.lendingRecord == null);
  }

  @Override
  public LendingRecord borrow(User borrower) {
    this.lendingRecord = new JpaLendingRecord();
    this.lendingRecord.setBorrower(borrower);
    this.lendingRecord.setStartDate(LocalDate.now());
    this.lendingRecord.setHoldingItem(this);
    return this.lendingRecord;
  }

  @Override
  public void returnItem() {
    this.lendingRecord = null;
  }

  @Override
  public LendingRecord getLendingRecord() throws ShareException {
    if (this.lendingRecord == null) {
      throw new ShareException("No LendingRecord for this HoldingItem.");
    }
    return this.lendingRecord;
  }

  @Override
  public boolean isRecalled() {
    return this.recall != null;
  }

  @Override
  public Recall getRecall() {
    return this.recall;
  }

  @Override
  public void setRecall(Recall recall) {
    this.recall = JpaHelper.toJpaRecall(recall);
  }

  @Override
  public boolean isReserved() {
    return this.reservation != null;
  }

  @Override 
  public void setReservation(Reservation reservation) {    
    this.reservation = JpaHelper.toJpaReservation(reservation);    
  }

  @Override
  public Reservation getReservation() {
    return this.reservation;
  }

  /**
   * Package private - to be used by convenience methods.
   */
  void setCatalogueEntry(JpaCatalogueEntry catEntry) {
    this.catalogueEntry = catEntry;
  }

}
