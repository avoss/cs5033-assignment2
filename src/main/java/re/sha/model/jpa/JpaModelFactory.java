package re.sha.model.jpa;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.glassfish.hk2.api.Factory;

import bitronix.tm.BitronixTransactionManager;
import bitronix.tm.Configuration;
import bitronix.tm.TransactionManagerServices;
import re.sha.model.Model;

/**
 * Provides instances of the {@link Model} on demand, e.g., for each instance of a RESTful API
 * class. Used with HK2 dependency injection.
 * 
 * @author alex.voss@st-andrews.ac.uk
 *
 */
public class JpaModelFactory implements Factory<Model> {

  private static final String DEFAULT_DSPROPS_FILENAME = "config/datasources.properties";
  private static String dsPropsFilename = DEFAULT_DSPROPS_FILENAME;

  protected static EntityManagerFactory emf = null;

  /**
   * Constructor that initialises the factory with the default filename
   */
  public JpaModelFactory() {    
    if (JpaModelFactory.emf == null) {
      try {
        this.setupJndi();
        this.setupTx(dsPropsFilename);
        BitronixTransactionManager tx = TransactionManagerServices.getTransactionManager();
        tx.begin(); // need to create EMFactory in transaction so it can create/update schema
        try {
          emf = Persistence.createEntityManagerFactory("ShaRePU");
        } catch (Exception e) {
          tx.rollback();
          throw new RuntimeException(e);
        }
        tx.commit();
      } catch (NotSupportedException | SystemException | SecurityException | IllegalStateException
          | RollbackException | HeuristicMixedException | HeuristicRollbackException e) {
        throw new RuntimeException("Error initialising model factory:", e);
      }
    }
  }
  
  public static void setDsPropsFilename(String filename) {
    dsPropsFilename = filename;
  }

  @Override
  public Model provide() {
    BitronixTransactionManager transactionManager =
        TransactionManagerServices.getTransactionManager();
    EntityManager entityManager = emf.createEntityManager();
    return new JpaModel(entityManager, transactionManager);
  }

  @Override
  public void dispose(Model model) {
    model.close();
  }

  /**
   * Sets up a Jndi context that the <a href="https://github.com/bitronix/btm">Bitronix</a> JTA
   * transaction manager can use to register the {@link DataSource}s.
   */
  void setupJndi() {
    System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
        "bitronix.tm.jndi.BitronixInitialContextFactory");
    try {
      new InitialContext();
    } catch (NamingException e) {
      throw new RuntimeException("Error initialising Jndi initial context:", e);
    }
  }

  /**
   * Sets up the Bitronix transaction manager using the given datasource properties file.
   */
  void setupTx(String dsPropsFilename) {
    Configuration conf = TransactionManagerServices.getConfiguration();
    conf.setResourceConfigurationFilename(dsPropsFilename);
    conf.setSkipCorruptedLogs(true);
  }
}
