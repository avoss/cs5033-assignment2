package re.sha.model.jpa;

import java.time.LocalDate;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

import re.sha.model.HoldingItem;
import re.sha.model.Recall;
import re.sha.model.User;

@Entity
@Access(AccessType.FIELD)
@Table(name = "RECALLS")
public class JpaRecall implements Recall {
  
  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;
  
  @ManyToOne
  @NotNull
  @Expose
  private JpaUser recaller = null;
  
  @NotNull  
  @Expose
  private LocalDate date;
  
  @OneToOne
  private JpaHoldingItem holdingItem;
  
  /**
   * For the benefit of Hibernate only.
   */
  protected JpaRecall() {  
  }
  
  public JpaRecall(User recaller, HoldingItem item, LocalDate date) {
      this.recaller = JpaHelper.toJpaUser(recaller);
      this.holdingItem = JpaHelper.toJpaHoldingItem(item);
      this.date = date;  
  }
  
  public JpaRecall(User recaller, HoldingItem item) {
    this(recaller, item, LocalDate.now());
  }

  @Override
  public User getRecaller() { 
    return this.recaller;
  }

  @Override
  public HoldingItem getHoldingItem() {
    return this.holdingItem;
  }

  @Override
  public LocalDate getRecallDate() {
    return this.date;
  }
}
