package re.sha.model.jpa;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

import re.sha.model.Notification;


/**
 * Jpa-mapped implementation of a {@link Notification}. 
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
@Entity
@Table(name = "NOTIFICATIONS")
@Access(AccessType.FIELD)
public class JpaNotification implements Notification {

  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;
  
  @Expose
  private String subject;
  
  @Expose
  private String message;
  
  @Expose
  private LocalDateTime timestamp;
  
  @ManyToOne
  private JpaUser user = null; 
  
  /**
   * For the benefit of Hibernate only.
   */
  protected JpaNotification() {}
  
  public JpaNotification(String subject, String message, JpaUser user) {
    this.subject = subject;
    this.message = message;
    this.timestamp = LocalDateTime.now();
    this.user = user;
  }
  
  @Override
  public String getSubject() {
    return this.subject;
  }

  @Override
  public String getMessage() {
    return this.message;
  }

  @Override
  public LocalDateTime getTimestamp() {
    return this.timestamp;
  }
}
