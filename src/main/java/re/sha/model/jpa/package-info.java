/**
 * The classes in this package implement the basic Sha.Re model in classes that have
 * <a href="http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html">JPA</a>
 * annotations and so can be readily mapped to a relational database using an object-relational
 * mapper (ORM) such as <a href="http://hibernate.org/">Hibernate</a>. Note that none of the code in
 * this package directly depends on Hibernate, so a different ORM can be chosen in the future.
 * 
 * A note on the use of interfaces and JPA: only classes can be mapped, so when we use interfaces in
 * our data access layer, we need to ensure that the ORM knows which concrete classes to expect so
 * that it can do its mapping work. One solution is to use classes in instance variables and allow
 * Hibernate field access while using interfaces in the return types of any methods (this is what I
 * am doing). Alternatively, we can specify a <tt>targetEntity</tt> argument in the annotations for
 * associations.
 * 
 * {@see <a href="https://en.wikibooks.org/wiki/Java_Persistence/Advanced_Topics#Interfaces">Java
 * Persistence</a>}
 * 
 */
package re.sha.model.jpa;
