package re.sha.model.jpa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.Notification;
import re.sha.model.User;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "USERS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) 
@DiscriminatorColumn(name = "USER_TYPE")
@DiscriminatorValue("U")
public class JpaUser implements User {

  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;

  @NotNull
  @Expose
  @Column(nullable = false)
  private String name = null;

  @NotNull
  @Expose
  @Column(nullable = false)
  private String surname = null;

  @NotNull
  @Expose
  @Column(nullable = false)
  private String title = null;

  @NotNull
  @Embedded
  @Expose
  @Column(unique = true)
  private InternetAddress email;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  private Set<JpaHoldingItem> holdingItems = new HashSet<>();

  @OneToMany(mappedBy = "borrower", fetch = FetchType.LAZY)
  private Set<JpaLendingRecord> lendings = new HashSet<>();

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  private List<JpaNotification> notifications = new ArrayList<>();

  /**
   * For the benefit of Hibernate only.
   */
  protected JpaUser() {}

  /**
   * Constructor that ensures that all required attributes are set.
   */
  public JpaUser(String title, String name, String surname, InternetAddress email) {
    this.setTitle(title);
    this.setName(name);
    this.setSurname(surname);
    this.setEMail(email);
  }

  /**
   * Convenience constructor that parses an email address from a string.
   */
  public JpaUser(String title, String name, String surname, String email) throws AddressException {
    this(title, name, surname, new InternetAddress(email));
  }

  @Override
  public long getId() {
    return this.id;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getSurname() {
    return this.surname;
  }

  @Override
  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Override
  public String getTitle() {
    return this.title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public void setEMail(InternetAddress email) {
    this.email = email;
  }

  @Override
  public InternetAddress getEMail() {
    return this.email;
  }

  @Override
  public void addHoldingItem(HoldingItem holdingItem) {
    if (holdingItem instanceof JpaHoldingItem) {
      this.holdingItems.add((JpaHoldingItem) holdingItem);
      holdingItem.setOwner(this);
    } else {
      throw new RuntimeException("Cannot mix implementations!");
    }
  }

  @Override
  public Set<HoldingItem> getHoldings() {
    return Collections.unmodifiableSet(this.holdingItems);
  }

  @Override
  public Set<LendingRecord> getLibraryRecord() {
    return Collections.unmodifiableSet(this.lendings);
  }

  @Override
  public void notify(Notification notification) {
    this.notifications.add(JpaHelper.toJpaNotification(notification));
  }

  @Override
  public List<Notification> getNotifications() {
    return Collections.unmodifiableList(this.notifications);
  }
}
