package re.sha.model.jpa;

import java.time.LocalDate;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.User;

@Entity
@Access(AccessType.FIELD)
@Table(name = "LENDING_RECORDS")
public class JpaLendingRecord implements LendingRecord {
  
  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;
    
  @ManyToOne
  @NotNull
  @Expose
  private JpaUser borrower = null;
  
  // this is a separate association rather than a bi-directional one matching lendingRecord
  // in JpaHoldingItem. This is so that we can keep a history of LendingRecords as well as 
  // a record of a current lending period. 
  @ManyToOne 
  @NotNull
  @Expose
  private JpaHoldingItem holdingItem = null;

  @NotNull
  @Expose
  private LocalDate startDate;

  @NotNull
  @Expose
  private LocalDate endDate;
    
  @Expose
  private int extended = 0;
  
  @Override
  public User getBorrower() {
    return this.borrower;
  }
  
  public void setBorrower(User borrower) {
    this.borrower = JpaHelper.toJpaUser(borrower);        
  }

  @Override
  public HoldingItem getHoldingItem() {
    return this.holdingItem ;
  }

  @Override
  public LocalDate getStartDate() {
    return this.startDate;
  }
  
  @Override
  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;    
  }

  @Override
  public LocalDate getEndDate() {
    return this.endDate;
  }
  
  @Override
  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }
  
  @Override
  public int getExtended() {
    return this.extended;
  }
  
  @Override
  public void incExtended() {
    this.extended++;
  }
  
  @Override
  public void setHoldingItem(HoldingItem holdingItem) {
    if(holdingItem instanceof JpaHoldingItem) {
      this.holdingItem = (JpaHoldingItem)holdingItem;  
    } else {
      throw new RuntimeException("Cannot mix implementations!");
    }
  }
}
