package re.sha.model.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;

import re.sha.exceptions.ReservationException;
import re.sha.exceptions.ShareException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.Model;
import re.sha.model.Reservation;
import re.sha.model.User;

/**
 * The purpose of this class is to provide data access methods that abstract away from the JPQL
 * queries required by the SPL. Create one instance of this per unit of work (such as a REST
 * request). In tests, you can replace this with a mock object.
 * 
 * Another purpose of this class is to contain a number of methods that create new persistent
 * objects. This is so that all the logic requiring access to the JPA API is centralised here and
 * our model objects can be POJOS (that just happen to have JPA annotations) and so can be reused
 * and unit-tested more easily.
 * 
 * The class is turned into a JPA Entity so that it can serve as a place for named queries. Why do
 * we not place the named queries on the other Entities? Because they may refer to more than one
 * Entity.
 * 
 * For more information on named queries and JPQL in general, see:
 * https://en.wikibooks.org/wiki/Java_Persistence/Querying
 * 
 * @author alex.voss@st-andrews.ac.uk
 *
 */
@Entity
@Table(name ="MODEL")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) 
@DiscriminatorColumn(name = "MODEL_TYPE")
@DiscriminatorValue("M")
@NamedQueries({@NamedQuery(name = "User.getByEMail",
    query = "select u from JpaUser as u where u.email.address = :email"),
  @NamedQuery(name = "User.getAll", query = "select u from JpaUser u"),
  @NamedQuery(name = "CatEntry.getAll", query = "select e from JpaCatalogueEntry e"),
  @NamedQuery(name = "Recall.getUserRecall", query="select r from JpaRecall r where recaller.id = :userid ")
})
public class JpaModel implements Model {

  @Id
  private long id = 0;

  int majorVersion = 1;
  int minorVersion = 0;

  @Transient
  protected EntityManager em = null;

  @Transient
  private TransactionManager tx = null;
  
  private boolean rollbackOnly = false;

  public JpaModel() {}

  public JpaModel(EntityManager em, TransactionManager tx) {
    this.em = em;
    this.tx = tx;
  }

  public void setEntityManager(EntityManager em) {
    this.em = em;
  }
  
  /**
   * Call this method if you want a model that will never commit data to the database but always
   * rolls back any transactions. This is useful for testing.
   */
  public void setRollbackOnly() throws IllegalStateException, SystemException {
    this.rollbackOnly = true;
  }

  /**
   * Closing the model simply closes the EntityManager, causing all objects to become detached.
   */
  public void close() {
    this.em.close();
  }

  @Override
  public User createUser(String title, String name, String surname, String email)
      throws ShareException, AddressException {
    User user = new JpaUser(title, name, surname, email);
    this.em.persist(user);
    return user;
  }

  @Override
  public User getUserById(long id) throws ShareException {
    return this.em.find(JpaUser.class, id);
  }

  @Override
  public User getUserByEmail(String email) {
    TypedQuery<User> query = em.createNamedQuery("User.getByEMail", User.class);
    query.setParameter("email", email);
    User user = query.getSingleResult();
    return user;
  }

  @Override
  public List<User> getUsers() {
    TypedQuery<User> query = em.createNamedQuery("User.getAll", User.class); 
    return query.getResultList();
  }

  @Override
  public void delete(User user) {
    this.em.remove(user);    
  }

  @Override
  public CatalogueEntry createCatalogueEntry(String idType, String identifier) {
    return this.createCatalogueEntry(idType, identifier, new HashMap<String,String>());
  }
  
  @Override
  public CatalogueEntry createCatalogueEntry(String idType, String identifier, Map<String, String> props) {
    CatalogueEntry entry = new JpaCatalogueEntry(idType, identifier);
    entry.addAllFields(props);
    this.em.persist(entry);
    return entry;
  }

  @Override
  public List<CatalogueEntry> getCatalogueEntries(int firstResult, int maxResults) {
    TypedQuery<CatalogueEntry> query = em.createNamedQuery("CatEntry.getAll", CatalogueEntry.class);
    query.setFirstResult(firstResult);
    query.setMaxResults(maxResults);
    return query.getResultList();
  }

  @Override
  public CatalogueEntry getCatalogueEntryById(long id) {    
    return this.em.find(JpaCatalogueEntry.class, id);
  }

  @Override
  public CatalogueEntry getCatalogueEntryByTypeAndIdentifier(final String idType, final String identifier) {
    TypedQuery<CatalogueEntry> query = em.createNamedQuery("CatEntry.getByIdAndType", CatalogueEntry.class);    
    return query.getSingleResult();
  }
  

  @Override
  public void delete(CatalogueEntry entry) {
    this.em.remove(entry);    
  }
  
  @Override
  public HoldingItem createHoldingItem(CatalogueEntry entry, User user) {
    HoldingItem item = entry.createHoldingItem(user);
    this.em.persist(item);
    return item;
  }
  
  @Override
  public HoldingItem getHoldingItem(long id) {
    return this.em.find(JpaHoldingItem.class, id);
  }

  @Override
  public LendingRecord borrow(HoldingItem item, User user) {
    LendingRecord lendingRecord = item.borrow(user);
    this.em.persist(lendingRecord);
    return lendingRecord;
  }

  /**
   * Returns the list of {@link LendingRecord}s that make up the lending history of this item. The
   * list is ordered by the start dates of the lending records.
   */
  public List<LendingRecord> getLendingHistory(HoldingItem holdingItem) {
    return null;
  }

  @Override
  public void notifyUser(User user, String subject, String message) {
    JpaUser jpaUser = JpaHelper.toJpaUser(user);
    JpaNotification notification = new JpaNotification(subject, message, jpaUser);
    user.notify(notification);
    this.em.persist(notification);
  }

  @Override
  public HoldingItem findUserRecall(CatalogueEntry entry, User user) { 
    // TODO: this programmatic approach is not nice, write a better query
    TypedQuery<JpaRecall> query =
        em.createNamedQuery("Recall.getUserRecall", JpaRecall.class);
    query.setParameter("userid", user.getId());
    List<JpaRecall> recalls = query.getResultList();
    for(JpaRecall recall: recalls) {
      HoldingItem item = recall.getHoldingItem();
      if(item.getCatalogueEntry() == entry) {
        return item;
      }
    }
    return null;
  }

  @Override
  public Reservation addReservation(CatalogueEntry entry, User user) {
    JpaReservation reservation = new JpaReservation(user, entry);
    entry.addReservation(reservation);
    this.em.persist(reservation);
    return reservation;
  }

  @Override
  public Reservation fulfillFirstReservation(CatalogueEntry catalogueEntry, HoldingItem item)
      throws ReservationException {
    Reservation reservation = catalogueEntry.fulfillFirstReservation();
    reservation.setHoldingItem(item);
    item.setReservation(reservation);
    this.em.persist(reservation);
    return reservation;
  }

  @Override
  public void beginTransaction() throws ShareException {
    try {
      this.tx.begin();      
    } catch (NotSupportedException | SystemException e) {
      throw new ShareException("Exception in transaction processing", e);
    }
  }

  @Override
  public void commitTransaction() throws ShareException {
    try {
      if(this.rollbackOnly) {
        this.tx.rollback();
      } else {
        this.tx.commit();
      }
    } catch (SecurityException | IllegalStateException | RollbackException | HeuristicMixedException
        | HeuristicRollbackException | SystemException e) {
      throw new ShareException("Exception in transaction processing", e);
    }
  }

  @Override
  public void rollbackTransaction() throws ShareException {
    try {
      this.tx.rollback();
    } catch (IllegalStateException | SecurityException | SystemException e) {
      throw new ShareException("Exception in transaction processing", e);
    }
  }

}
