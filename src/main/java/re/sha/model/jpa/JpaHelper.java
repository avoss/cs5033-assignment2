package re.sha.model.jpa;

import re.sha.exceptions.MixedImplementationException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.Notification;
import re.sha.model.Recall;
import re.sha.model.Reservation;
import re.sha.model.User;

/**
 * Contains helper methods that try to cast a given object to a JPA implementation class. If passed
 * an object that is not an instance of a JPA entity class, they will simply throw a
 * {@link MixedImplementationException}.
 */
public abstract class JpaHelper {

  public static JpaUser toJpaUser(User user) {
    if (user instanceof JpaUser || user == null) {
      return (JpaUser) user;
    } else {
      throw new MixedImplementationException();
    }
  }

  public static JpaReservation toJpaReservation(Reservation reservation) {
    if (reservation instanceof JpaReservation || reservation == null) {
      return (JpaReservation) reservation;
    } else {
      throw new MixedImplementationException();
    }
  }

  public static JpaRecall toJpaRecall(Recall recall) {
    if (recall instanceof JpaRecall || recall == null) {
      return (JpaRecall) recall;
    } else {
      throw new MixedImplementationException();
    }
  }

  public static JpaCatalogueEntry toJpaCatalogueEntry(CatalogueEntry entry) {
    if (entry instanceof JpaCatalogueEntry || entry == null) {
      return (JpaCatalogueEntry) entry;
    } else {
      throw new MixedImplementationException();
    }
  }

  public static JpaHoldingItem toJpaHoldingItem(HoldingItem item) {
    if (item instanceof JpaHoldingItem || item == null) {
      return (JpaHoldingItem) item;
    } else {
      throw new MixedImplementationException();
    }
  }

  public static JpaNotification toJpaNotification(Notification notification) {
    if (notification instanceof JpaNotification || notification == null) {
      return (JpaNotification) notification;
    } else {
      throw new MixedImplementationException();
    }
  }
}
