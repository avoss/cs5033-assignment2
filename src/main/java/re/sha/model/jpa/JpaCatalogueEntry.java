package re.sha.model.jpa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

import re.sha.exceptions.ReservationException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.Reservation;
import re.sha.model.User;

/**
 * Basic implementation of a {@link CatalogueEntry}, which consists of a (key,value) mapping. If a
 * more sophisticated model is required, you can create a subclass that adds the required behaviour.
 * An example might be a {@link CatalogueEntry} that contains a set of images such as
 * {@link JpaImageCatalogueEntry}.
 * 
 * @author alex.voss@st-andrews.ac.uk
 *
 */
@Entity
@Table(name = "CATALOGUE_ENTRIES")
@Access(AccessType.FIELD)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) 
@DiscriminatorColumn(name = "CE_TYPE")
@DiscriminatorValue("E")
public class JpaCatalogueEntry implements CatalogueEntry {

  @Id
  @Expose
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id = 0;

  @NotNull
  @Expose
  @Column(updatable = false, nullable = false)
  private String identifier = null;

  @NotNull
  @Expose
  @Column(updatable = false, nullable = false)
  private String identifierType = null;

  @OneToMany(mappedBy = "catalogueEntry", cascade = CascadeType.PERSIST)
  private Set<JpaHoldingItem> holdings = new HashSet<>();

  @OneToMany(mappedBy = "catalogueEntry", cascade = CascadeType.PERSIST)
  private List<JpaReservation> reservations = new ArrayList<>();

  @Expose
  @ElementCollection
  private Map<String, String> properties = new HashMap<String, String>();

  /**
   * For the benefit of Hibernate only.
   */
  protected JpaCatalogueEntry() {}

  /**
   * Use this constructor to ensure that all the fields that cannot be null are initialised.
   */
  public JpaCatalogueEntry(String identifierType, String identifier) {
    this.identifier = identifier;
    this.identifierType = identifierType;
  }

  @Override
  public long getId() {
    return this.id;
  }

  @Override
  public String getIdentifier() {
    return this.identifier;
  }

  @Override
  public String getIdentifierType() {
    return this.identifierType;
  }

  @Override
  public Set<String> getKeys() {
    return Collections.unmodifiableSet(this.properties.keySet());
  }

  @Override
  public String getValue(String key) {
    return this.properties.get(key);
  }

  @Override
  public void addField(String key, String value) {
    this.properties.put(key, value);
  }
  
  @Override
  public void addAllFields(Map<String, String> props) {
    this.properties.putAll(props);    
  }

  @Override
  public HoldingItem createHoldingItem(User user) {
    JpaHoldingItem hi = new JpaHoldingItem();
    hi.setCatalogueEntry(this);
    this.holdings.add(hi);
    user.addHoldingItem(hi);
    return hi;
  }

  @Override
  public Set<HoldingItem> getHoldings() {
    return Collections.unmodifiableSet(this.holdings);
  }

  @Override
  public Set<HoldingItem> getAvailableHoldingItems() {
    Set<HoldingItem> available = new HashSet<HoldingItem>();
    for (HoldingItem holdingItem : this.holdings) {
      if (holdingItem.isAvailable()) {
        available.add(holdingItem);
      }
    }
    return Collections.unmodifiableSet(available);
  }

  @Override
  public void addReservation(Reservation reservation) {    
      this.reservations.add(JpaHelper.toJpaReservation(reservation));  
  }

  @Override
  public boolean isReserved() {
    return !this.reservations.isEmpty();
  }

  @Override
  public List<Reservation> getReservations() {
    return Collections.unmodifiableList(this.reservations);
  }

  @Override
  public Reservation fulfillFirstReservation() throws ReservationException {
    if(this.reservations.isEmpty()) {
      throw new ReservationException("No reservation.");
    }
    Reservation reservation = this.reservations.get(0);
    this.reservations.remove(0);
    reservation.setCatalogueEntry(null);
    return reservation;
  }

}
