package re.sha.model;

import java.time.LocalDateTime;

import re.sha.exceptions.ShareException;

/**
 * Represents a reservation made for a {@link CatalogueEntry}. As soon as a {@link HoldingItem} for
 * it become available, the oldest reservation in the system is fulfilled.
 */
public interface Reservation {

  /**
   * Gets the user who made this reservation.
   */
  User getUser();

  /**
   * Returns the date the reservation was made.
   */
  LocalDateTime getTimestamp();

  /**
   * Gets the {@link CatalogueEntry} that this reservation is for or null if it is for a
   * {@link HoldingItem}.
   */
  CatalogueEntry getCatalogueEntry();

  /**
   * Set the {@link CatalogueEntry} that is being reserved.
   */
  void setCatalogueEntry(CatalogueEntry entry);
  
  /**
   * Gets the {@link HoldingItem} that this reservation is for or null if it is for a
   * {@link CatalogueEntry}.
   */
  HoldingItem getHoldingItem();

  /**
   * Called when a {@link HoldingItem} has become available that can fulfill the reservation.
   */
  void setHoldingItem(HoldingItem item);

  /**
   * Checks if a {@link HoldingItem} has been returned to the library and assigned as the one
   * fulfilling this reservation.
   */
  boolean isFulfilled();

  /**
   * Gets the {@link HoldingItem} that has been returned and is fulfilling this reservation.
   * 
   * @throws {@link LibraryException} if the reservation is not fulfilled.
   */
  HoldingItem getAvailableItem() throws ShareException;

  /**
   * Gets the date the reservation was expires if {@link isFulfilled()} returns true.
   * 
   * @throws {@link LibraryException} if the reservation is not fulfilled.
   */
  LocalDateTime getExpiryDateTime() throws ShareException;

  /**
   * Checks if the expiry time has passed, so the {@link HoldingItem} can be assigned to a different
   * {@link User}.
   * 
   * @returns true if the reservation is fulfilled and the expiry time has passed, false otherwise.
   */
  boolean isExpired();

}
