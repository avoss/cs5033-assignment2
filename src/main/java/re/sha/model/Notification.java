package re.sha.model;

import java.time.LocalDateTime;

/**
 * A simple notification mechanism that can be mapped by implementations to a number
 * of notification channels (within the web application, email, etc.)
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public interface Notification {

  /**
   * Get the subject line of the notification.
   */
  String getSubject();
  
  /**
   * Get the body text of the message. 
   */
  String getMessage();
  
  /**
   * Get the time the notification was sent.
   */
  LocalDateTime getTimestamp();
  
}
