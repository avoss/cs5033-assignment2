package re.sha.model;

import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;

import re.sha.exceptions.ReservationException;
import re.sha.exceptions.ShareException;

/**
 * Main access class for model elements and operations that create persistent model elements.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public interface Model {

  /**
   * Get a list of users.
   */
  List<User> getUsers();

  /**
   * Get a {@link User} by database id.
   */
  User getUserById(long id) throws ShareException;

  /**
   * Get a {@link User} by their unique email address.
   */
  User getUserByEmail(String email);

  /**
   * Delete a {@link User} from the system. 
   */
  void delete(User user);

  /**
   * Create a new {@link CatalogueEntry} given only its identifier and id type.
   */
  CatalogueEntry createCatalogueEntry(String idType, String identifier);

  /**
   * Create a new {@link CatalogueEntry} given its identifier, id type and a {@link Map} of properties. 
   */
  CatalogueEntry createCatalogueEntry(String idType, String identifier, Map<String, String> props);

  /**
   * Get a list of {@link CatalogueEntries}. The arguments allow paging of the results by specifying
   * the first result (as an index into the full list of results) and the number of results to
   * return.
   */
  List<CatalogueEntry> getCatalogueEntries(int firstResult, int maxResults);

  /**
   * Get a single {@link CatalogueEntry} based on its database id.
   */
  CatalogueEntry getCatalogueEntryById(long id);

  /**
   * Get a single {@link CatalogueEntry} based on its identifier and identifier type. This can be
   * used to implement lookup using numbering schemes such as ISBN numbers.
   */
  CatalogueEntry getCatalogueEntryByTypeAndIdentifier(String idType, String identifier);
  
  /**
   * Delete a {@link CatalogueEntry} from the system. 
   */
  void delete(CatalogueEntry entry);
  
  /**
   * Create a new {@link HoldingItem} for the given {@link CatalogueEntry} and {@link User}.
   */
  HoldingItem createHoldingItem(CatalogueEntry entry, User user);

  /**
   * Get the {@link HoldingItem} with the given database id.
   */
  HoldingItem getHoldingItem(long id);
  
  /**
   * Returns the list of {@link LendingRecord}s that make up the lending history of this item. The
   * list is ordered by the start dates of the lending records.
   */
  List<LendingRecord> getLendingHistory(HoldingItem holdingItem);

  /**
   * Create a notification for the given user.
   */
  void notifyUser(User user, String subject, String message);

  /**
   * Borrow the given {@link HoldingItem} for the given {@link User}. Use this method rather than
   * calling {@link HoldingItem#borrow(User)} to ensure that the {@link LendingRecord} is persisted.
   */
  LendingRecord borrow(HoldingItem item, User user);

  /**
   * Add a reservation to the given {@link CatalogueEntry} for the given {@link User}.
   */
  Reservation addReservation(CatalogueEntry entry, User user);

  /**
   * Find any {@link Recall}s the given {@link User} may have placed for the given
   * {@link CatalogueEntry}.
   */
  HoldingItem findUserRecall(CatalogueEntry entry, User user);

  /**
   * Given a {@link CatalogueEntry} and a {@link HoldingItem} for it, transfer the oldest
   * {@link Reservation} from the former to the latter, so there is a specific item reserved for the
   * {@link User}.
   */
  Reservation fulfillFirstReservation(CatalogueEntry catalogueEntry, HoldingItem item)
      throws ReservationException;

  /**
   * It depends on the implementation what 'closing' a Model means.
   */
  void close();

  /**
   * Create a new {@link User} given required attributes.
   */
  User createUser(String title, String name, String surname, String email)
      throws ShareException, AddressException;

  /**
   * Begin a transaction (which is bound to the executing thread).
   */
  void beginTransaction() throws ShareException;

  /**
   * Commit a transaction started with {@link #beginTransaction()}.
   */
  void commitTransaction() throws ShareException;

  /**
   * Rollback a transaction started with {@link #beginTransaction()}.
   */
  void rollbackTransaction() throws ShareException;

}
