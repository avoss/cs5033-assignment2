package re.sha.model;

import java.time.LocalDate;

/**
 * Represents a recall placed on a {@link HoldingItem}. Note that a user issues
 * a recall on a {@link CatalogueEntry} and the system figures out which of its 
 * associated {@link HoldingItem}s to recall. This allows the implementation to
 * be fair or to implement specific priorities.
 */
public interface Recall {
  
  /**
   * Get the user who placed this recall.
   */
  User getRecaller();
  
  /**
   * Get the {@link HoldingItem} that was selected by the system for recall.
   */
  HoldingItem getHoldingItem();
  
  /**
   * Get the date the recall was placed.
   */
  LocalDate getRecallDate();
}
