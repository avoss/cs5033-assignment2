package re.sha.model;

import re.sha.exceptions.ShareException;

/**
 * A HoldingItem represents a physical artifact in the library. Each HoldingItem has an associated
 * {@link CatalogueEntry} that contains the bibliographic information for it, a {@link User} who
 * owns the item and an accession number that uniquely identifies the it.
 */
public interface HoldingItem {

  /**
   * Get the database id.
   */
  long getId();

  /**
   * Returns the accession number for this item. The accession number encodes the date of
   * acquisition in the lower-most digits in YYYYMMDD format.
   */
  long getAccessionNumber();

  /**
   * Returns the {@link CatalogueEntry} that contains the bibliographic information for this item.
   */
  CatalogueEntry getCatalogueEntry();

  /**
   * Returns the location of the item, the place where it normally resides.
   */
  String getLocation();

  /**
   * Sets the location of the item, the place where it normally resides.
   */
  void setLocation(String location);

  /**
   * Get the owner of this item.
   */
  User getOwner();

  /**
   * Set the owner of this item.
   */
  void setOwner(User user);

  /**
   * Checks if the item is available for lending.
   */
  boolean isAvailable();

  /**
   * Creates a {@link LendingRecord} for this item that marks it as being borrowed by a user and
   * returns this.
   */
  LendingRecord borrow(User borrower);

  /**
   * Call when the item is returned. Simply deletes the {@link LendingRecord}, other activities
   * should be done in the business layer.
   */
  void returnItem();

  /**
   * For an item that is currently unavailable return the {@link LendingRecord} that contains the
   * information about the current lending episode. Throws a ShareException if the item is not
   * currently lent out.
   */
  LendingRecord getLendingRecord() throws ShareException;

  /**
   * Checks if an item is recalled by the owner.
   */
  boolean isRecalled();

  /**
   * Returns the {@link Recall}
   */
  Recall getRecall();

  /**
   * Sets a {@link Recall} on this HoldingItem.
   */
  void setRecall(Recall jpaRecall);

  /**
   * Checks if this item is reserved. 
   */
  boolean isReserved();

  /**
   * Sets the Reservation for the item. 
   */
  void setReservation(Reservation reservation);

  /**
   * Gets any Reservation that may apply to this item. 
   */
  Reservation getReservation();

}
