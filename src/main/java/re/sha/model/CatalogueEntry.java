package re.sha.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

import re.sha.exceptions.ReservationException;

/**
 * A bibliographic entry is a description of a published piece of work but not of items the library
 * actually holds.
 */
public interface CatalogueEntry {
  
  /**
   * Get the database identifier for this catalogue entry.
   */
  long getId();
  
  /**
   * Get the identifier for the given CatalogueEntry. This could be an ISBN number or another kind of
   * identifier, {@see getIdType()}.  
   */
  String getIdentifier();
  
  /**
   * Get the type of identifier associated with this CatalogueEntry.
   */
  String getIdentifierType();

  /**
   * Returns the keys that define the fields in the bibliographic information stored. Note that
   * these are not ordered.
   */
  Set<String> getKeys();

  /**
   * Returns the value stored under a given key.
   */
  String getValue(String key);

  /**
   * Adds a value for the given key. If a value has already been stored under this key then it will
   * be overwritten by the new one.
   */
  void addField(String key, String value);
  
  /**
   * Adds properties from the given {@link Map}. Any value that already exists under a key in the input
   * is overwritten.
   */
  void addAllFields(Map<String, String> props);

  /**
   * Create a new HoldingItem object for this CatalogueEntry. The HoldingItem created will be linked to
   * the CatalogueEntry, so it will appear in the set returned by {@link getHoldings()} and likewise the
   * {@link HoldingItem#getCatalogueEntry()} method will return this CatalogueEntry. It will also be linked to
   * the given {@link User} to ensure that a {@link HoldingItem} is always owned by someone.  
   */
  HoldingItem createHoldingItem(User user);

  /**
   * Returns the set of items that the library holds for this CatalogueEntry.
   */
  Set<HoldingItem> getHoldings();
  
  /**
   * Returns the set of items that the library holds for this CatalogueEntry and that
   * are currently available for lending.
   */
  Set<HoldingItem> getAvailableHoldingItems();  

  /**
   * Add a {@link Reservation} for the CatalogueEntry. There can be any number of reservations for a
   * CatalogueEntry and they are store in the order in which they are made, so they form a
   * first-come-first-served queue.
   */
  void addReservation(Reservation reservation);
  
  /**
   * Checks if there is a reservation on this CatalogueEntry.
   */
  boolean isReserved();

  /**
   * Retrieves the list of {@Reservation}s, ordered by when they were made.
   */
  List<Reservation> getReservations();

  /**
   * Fulfill the first reservation, removing it from the list. It will instead be placed on
   * an appropriate {@link HoldingItem} until a user borrows this item or the Reservation 
   * expires.
   */
  Reservation fulfillFirstReservation() throws ReservationException;

}
