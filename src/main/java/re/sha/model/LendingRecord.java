package re.sha.model;

import java.time.LocalDate;

/**
 * A record describing a single borrowing act. We keep a complete history of all these
 * records in the library system to be able to analyse the lending history of a particular 
 * {@link CatalogueEntry} even after a {@link HoldingItem} is removed from the library. 
 */
public interface LendingRecord {
  
  /**
   * Returns the user who borrows/borrowed an item. 
   */
  User getBorrower();
  
  /**
   * Sets the user who borrows an item.
   */
  void setBorrower(User borrower);
  
  /**
   * Get the {@link HoldingItem} that was borrowed.
   */
  HoldingItem getHoldingItem();
  
  /**
   * Get the start date for the lending period this record describes.
   */
  LocalDate getStartDate();
  
  /**
   * Set the start date for the lending period this record describes.
   * @param startDate
   */
  void setStartDate(LocalDate startDate);
  
  /**
   * Get the end date for the lending period this record describes.
   */
  LocalDate getEndDate();

  /**
   * Get the number of times the lending period has been extended.
   */
  int getExtended();
  
  /**
   * Increment the number of times the lending period has been extended.
   */
  void incExtended();

  /**
   * Set the end date for the lending period this record describes.
   */
  void setEndDate(LocalDate endDate);

  /**
   * Set the {@link HoldingItem} that is being lent out.
   */
  void setHoldingItem(HoldingItem holdingItem);

  
}