package re.sha.model;

import java.util.List;
import java.util.Set;

import javax.mail.internet.InternetAddress;

/**
 * Representation of a user of the system. Users can be owners of {@link HoldingItem}s and can
 * borrow such items from other users.
 */
public interface User {
  
  /**
   * Returns the ID of the User.
   */
  long getId();
  
  /**
   * The first (given) name of the user.
   */
  String getName();
  
  /**
   * Set the first (given) name of the user.
   */
  void setName(String name);
  
  /**
   * Get the surname (family name) of the user.
   */
  String getSurname();
  
  /**
   * Set the surname (family name) of the user.
   */
  void setSurname(String name);
  
  /**
   * Get the title of the user.
   */
  String getTitle();
  
  /**
   * Set the title of the user.
   */
  void setTitle(String title);
  
  /**
   * Set the email address of the user.
   */
  void setEMail(InternetAddress email); 
  
  /**
   * Get the email address of the user.
   */
  InternetAddress getEMail();
  
  /**
   * Add a new {@link HoldingItem} to the list of items this user owns.
   */
  void addHoldingItem(HoldingItem holdingItem);
  
  /**
   * Returns the {@link HoldingItem}s a user owns.
   */
  Set<HoldingItem> getHoldings();
  
  /**
   * Returns the currently active {@link LendingRecord}s that make up that
   * users Library Record.
   */
  Set<LendingRecord> getLibraryRecord();

  /**
   * Record a notification for this user.
   */
  void notify(Notification notification);  
  
  /**
   * Get notifications.
   */
  List<Notification> getNotifications();

}