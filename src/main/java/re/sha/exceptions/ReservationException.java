package re.sha.exceptions;

public class ReservationException extends ShareException {

  private static final long serialVersionUID = 1L;

  public ReservationException(String msg) {
    super(msg);
  }
  
  public ReservationException(String msg, Exception e) {
    super(msg, e);
  }
}
