package re.sha.exceptions;

public class ShareException extends Exception {

  private static final long serialVersionUID = 1L;

  public ShareException(String msg) {
    super(msg);
  }

  public ShareException(String msg, Exception e) {
    super(msg,e);
  }
}