package re.sha.exceptions;

public class MixedImplementationException extends IllegalArgumentException {
 
  private static final long serialVersionUID = 1L;

  public MixedImplementationException() {
    super("Cannot mix implementations!");
  }
}
