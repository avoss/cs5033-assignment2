package re.sha.exceptions;

public class RecallException extends ShareException {

  private static final long serialVersionUID = 1L;

  public RecallException(String msg) {
    super(msg);   
  }

  public RecallException(String msg, Exception e) {
    super(msg,e);
  }
}