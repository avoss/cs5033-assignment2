package re.sha.exceptions;

public class TooManyItemsException extends ShareException {

  private static final long serialVersionUID = 1L;

  public TooManyItemsException(String msg) {
    super(msg);  
  }
  
  public TooManyItemsException(String msg, Exception e) {
    super(msg, e);
  }
  

}
