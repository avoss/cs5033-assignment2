package re.sha.exceptions;


public class NoHoldingException extends ShareException{
  
  private static final long serialVersionUID = 1L;

  public NoHoldingException(String msg) {
    super(msg);
  }
  
  public NoHoldingException(String msg, Exception e) {
    super(msg, e);
  }

}
