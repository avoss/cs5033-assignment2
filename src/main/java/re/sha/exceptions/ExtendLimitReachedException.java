package re.sha.exceptions;

public class ExtendLimitReachedException extends ShareException {

  private static final long serialVersionUID = 1L;

  public ExtendLimitReachedException(String msg) {
    super(msg);    
  }
  
  public ExtendLimitReachedException(String msg, Exception e) {
    super(msg, e);    
  }

}
