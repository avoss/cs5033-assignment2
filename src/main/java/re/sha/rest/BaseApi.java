package re.sha.rest;

import java.lang.reflect.Type;
import java.time.LocalDate;

import javax.inject.Inject;
import javax.mail.internet.InternetAddress;

import org.glassfish.hk2.api.PostConstruct;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import re.sha.business.LendingRules;
import re.sha.business.RecallRules;
import re.sha.business.ReservationRules;
import re.sha.model.Model;

/**
 * A simple base class for Api implementations that creates a configured {@link Gson} instance for the
 * handling of JSON serialisation in the APIs. The main purpose is to configure it to exclude any fields
 * in serialisation that are not explicitly exposed with an @Expose annotation. This way, bidirectional
 * associations do not cause a problem in Gson serialisation.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public class BaseApi implements PostConstruct {  

  @Inject
  protected Model model = null;

  @Inject
  protected LendingRules lendingRules = null;

  @Inject
  protected RecallRules recallRules = null;

  @Inject
  protected ReservationRules reservationRules = null;

  protected Gson gson = null;

  /**
   * Creates a Gson instance that is configured with a number of type converters as well as to
   * exclude fields from serialisation that are not annotated with @Expose.
   */
  public BaseApi() {
    GsonBuilder builder = new GsonBuilder();
    builder = builder.excludeFieldsWithoutExposeAnnotation();
    
    builder.registerTypeAdapter(LocalDate.class, new JsonSerializer<LocalDate>(){
      @Override
      public JsonElement serialize(LocalDate src, Type typeOfSrc,
          JsonSerializationContext context) {
        return new JsonPrimitive(src.toString());
      }      
    });
    
    builder.registerTypeAdapter(InternetAddress.class, new JsonSerializer<InternetAddress>() {
      @Override
      public JsonElement serialize(InternetAddress src, Type typeOfSrc,
          JsonSerializationContext context) {
        return new JsonPrimitive(src.toString());
      }      
    });
    
    this.gson = builder.create();
  }

  @Override
  public void postConstruct() {
    this.lendingRules.setModel(this.model);
    this.recallRules.setModel(this.model);
    this.reservationRules.setModel(this.model);
  }
  
}
