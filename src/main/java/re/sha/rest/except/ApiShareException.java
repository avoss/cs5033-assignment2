package re.sha.rest.except;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import re.sha.exceptions.ShareException;

public class ApiShareException extends WebApplicationException {

  private static final long serialVersionUID = 1L;

  /**
   * Create a HTTP 400 (Bad Request) exception for a ShareException.
   */
  public ApiShareException() {
    super(Response.Status.BAD_REQUEST);
  }

  /**
   * Create a HTTP 500 (Internal Server Error) exception for a ShareException.
   * 
   * @param message the String that is the entity of the 400 response.
   */
  public ApiShareException(String message) {
    super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(message).type("text/plain")
        .build());
  }

  /**
   * Create a HTTP 500 (Internal Server Error) exception for a ShareException.
   * 
   * @param message the String that is the entity of the 500 response.
   */
  public ApiShareException(String message, ShareException e) {
    super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(constructEntity(message, e))
        .type("text/plain").build());
  }

  /**
   * Construct a suitable message from an error message given and an underlying exception. Include
   * the stack trace for better error reporting. 
   */
  private static String constructEntity(String message, ShareException e) {
    StringWriter stringWriter = new StringWriter();
    e.printStackTrace(new PrintWriter(stringWriter));    
    return message + "\n" + "Underlying cause:\n" + e.getMessage() + "\nStack Trace:\n"
        + stringWriter.toString();
  }


}
