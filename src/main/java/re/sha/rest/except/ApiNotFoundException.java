package re.sha.rest.except;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ApiNotFoundException extends WebApplicationException {
 
  private static final long serialVersionUID = 1L;

  /**
   * Create a HTTP 404 (Not Found) exception.
   */
   public ApiNotFoundException() {
     super(Response.Status.NOT_FOUND);
   }
  
   /**
   * Create a HTTP 404 (Not Found) exception.
   * @param message the String that is the entity of the 404 response.
   */
   public ApiNotFoundException(String message) {
     super(Response.status(Response.Status.NOT_FOUND).entity(message).type("text/plain").build());
   }
}
