package re.sha.rest;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import re.sha.exceptions.ShareException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.Model;
import re.sha.model.Reservation;
import re.sha.model.User;
import re.sha.rest.except.ApiNotFoundException;

/**
 * RESTful API for managing {@link CatalogueEntry} instances. The following list describes what
 * resources and methods are available:
 * 
 * <ul>
 * <li><tt>PUT /entry/{idType}/{identifier}</tt> - create a new catalogue entry using an identifier
 * type and identifier.</li>
 * <li><tt>GET /entry/all/{first}/{max}</tt> - get all catalogue entries. To support paging, you
 * need to provide the index of the first entry to return from the list of results as well as the
 * maximum number of results to return.</li>
 * <li><tt>GET /entry/{id}</tt> - get a specific catalogue entry given its database id (which is
 * different from the identifier field as it is assigned by the underlying database)</li>
 * <li><tt>GET /entry/{idType}/{identifier}</tt> - get a specific catalogue entry using the
 * identifier type and identifier given on creation.</li>
 * <li><tt>DELETE /entry/{id}</tt> - delete the catalogue entry with the given id.</li>
 * </ul>
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
@Path("/entry")
public class CatalogueEntryApi extends BaseApi {

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{idType}/{identifier}")
  public final Response createCatalogueEntry(@PathParam("idType") final String idType,
      @PathParam("identifier") final String identifier) throws ShareException {

    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.createCatalogueEntry(idType, identifier);
      String data = gson.toJson(entry) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("all/{first}/{max}")
  public final Response getCatalogueEntries(@PathParam("first") int firstResult,
      @PathParam("max") int maxResults) throws ShareException {
    try {
      this.model.beginTransaction();
      List<CatalogueEntry> entries = this.model.getCatalogueEntries(firstResult, maxResults);
      String data = gson.toJson(entries) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public final Response getCatalogueEntry(@PathParam("id") final long id) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(id);
      if (entry == null) {
        throw new ApiNotFoundException("catalogue entry could not be found");
      }
      String data = gson.toJson(entry) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{type}/{id}")
  public final Response getCatalogueEntryByTypeAndId(@PathParam("type") final String idType,
      @PathParam("id") final String identifier) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryByTypeAndIdentifier(idType, identifier);
      if (entry == null) {
        throw new ApiNotFoundException("catalogue entry could not be found");
      }
      String data = gson.toJson(entry) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @DELETE
  @Path("{id}")
  public final Response deleteCatalogueEntry(@PathParam("id") long id) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(id);
      this.model.delete(entry);
      this.model.commitTransaction();
      this.model.close();
      return Response.ok().build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/newHoldingItem/user/{userId}")
  public final Response createHoldingItem(@PathParam("id") long catEntryId,
      @PathParam("userId") long userId) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(catEntryId);
      if (entry == null) {
        throw new ApiNotFoundException("catalogue entry not found");
      }
      User user = this.model.getUserById(userId);
      if (user == null) {
        throw new ApiNotFoundException("user not found");
      }
      HoldingItem item = this.model.createHoldingItem(entry, user);
      String data = gson.toJson(item) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/holdings")
  public final Response getHoldings(@PathParam("id") long id) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(id);
      if (entry == null) {
        throw new NotFoundException("Catalogue entry " + id + " not found.");
      }
      Set<HoldingItem> items = entry.getHoldings();
      String data = this.gson.toJson(items);
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/reservation/user/{userId}")
  public final Response createReservation(@PathParam("id") long id,
      @PathParam("userId") long userId) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(id);
      if (entry == null) {
        throw new NotFoundException("Could not find the catalogue entry.");
      }
      User user = this.model.getUserById(userId);
      if (user == null) {
        throw new NotFoundException("Could not find the user.");
      }
      Reservation reservation = this.reservationRules.makeReservation(user, entry);
      String data = gson.toJson(reservation) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/reservation")
  public final Response getReservations(@PathParam("id") long id) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(id);
      if (entry == null)
        throw new NotFoundException();
      List<Reservation> reservations = entry.getReservations();
      String data = gson.toJson(reservations) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/borrow/user/{userId}")
  public final Response borrowCatalogueEntry(@PathParam("id") long id,
      @PathParam("userId") long userId) throws ShareException {
    try {
      this.model.beginTransaction();
      CatalogueEntry entry = this.model.getCatalogueEntryById(id);
      if (entry == null) {
        throw new ApiNotFoundException("holding item not found");
      }
      User user = this.model.getUserById(userId);
      if (user == null) {
        throw new ApiNotFoundException("user not found");
      }
      LendingRecord lr = this.lendingRules.borrow(user, entry);
      String data = gson.toJson(lr) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }
}
