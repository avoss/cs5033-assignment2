package re.sha.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import re.sha.exceptions.ShareException;
import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.User;
import re.sha.rest.except.ApiNotFoundException;

@Path("/item")
public class HoldingItemApi extends BaseApi {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public final Response getHoldingItem(@PathParam("id") final long id) throws ShareException {
    try {
      this.model.beginTransaction();
      HoldingItem item = this.model.getHoldingItem(id);
      if (item == null) {
        throw new ApiNotFoundException("holding item not found");
      }
      String data = gson.toJson(item) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch (Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }

 
}
