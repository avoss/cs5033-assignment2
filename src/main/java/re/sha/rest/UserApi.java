package re.sha.rest;

import java.util.List;

import javax.inject.Inject;
import javax.mail.internet.AddressException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import re.sha.exceptions.ShareException;
import re.sha.model.Model;
import re.sha.model.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * RESTful API for managing {@link User} instances. The following list describes what 
 * resources and methods are available:
 * 
 * <ul>
 * <li><tt>GET /user/{id}</tt> - get the user with the given database id.</li>
 * <li><tt>GET /user/byEmail/{email}</tt> - get the user with the given email address.</li>
 * <li><tt>PUT /user/{title}/{name}/{surname}/{email}</tt> - create a new user given essential information.</li>
 * <li><tt>DELETE /user/{id}</tt> - delete the user with the given database id.</li>
 * </ul>
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
@Path("/user")
public class UserApi extends BaseApi {
    
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public final Response getUsers() throws ShareException {
    try {
      this.model.beginTransaction();
      List<User> users = this.model.getUsers();
      String data = gson.toJson(users) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch(Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }
  
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public final Response getUser(@PathParam("id") final long id) throws ShareException {
    try {
      this.model.beginTransaction();
      User user = this.model.getUserById(id);
      if(user == null) {
        this.model.commitTransaction();
        return Response.status(Response.Status.NOT_FOUND).build();
      } else {
        String data = gson.toJson(user) + "\n";
        this.model.commitTransaction();
        this.model.close();
        return Response.ok(data).build();
      }      
    } catch(Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }
  
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("byEmail/{email}")
  public final Response getUserByEmail(@PathParam("email") final String email) throws ShareException {
    try {
      this.model.beginTransaction();
      User user = this.model.getUserByEmail(email);
      String data = gson.toJson(user) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build();
    } catch(Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }
  
  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{title}/{name}/{surname}/{email}")
  public final Response createUser(
      @PathParam("title") final String title,
      @PathParam("name") final String name,
      @PathParam("surname") final String surname,
      @PathParam("email") final String email
  ) throws AddressException, ShareException { 
    try {
      this.model.beginTransaction();
      User user = this.model.createUser(title, name, surname, email);
      String data = gson.toJson(user) + "\n";
      this.model.commitTransaction();
      this.model.close();
      return Response.ok(data).build(); 
    } catch(Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }        
  }
  
  @DELETE
  @Path("{id}")
  public final Response deleteUserByName(@PathParam("id") final long id) throws ShareException {
    try {
      this.model.beginTransaction();
      User user = this.model.getUserById(id); 
      this.model.delete(user);
      this.model.commitTransaction();
      this.model.close();
      return Response.ok().build();
    } catch(Exception e) {
      this.model.rollbackTransaction();
      this.model.close();
      throw e;
    }
  }
}
