package re.sha.business;

import re.sha.exceptions.RecallException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.Model;
import re.sha.model.Recall;
import re.sha.model.User;

/**
 * Defines methods for managing {@link Recall}s.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public interface RecallRules {
  
  /**
   * Set reference to the {@link Model} implementation.
   */
  void setModel(Model model);

  /**
   * Recall a {@link HoldingItem} for the given {@link CatalogueEntry}. It is up to the
   * implementation to choose which {@link HoldingItem} gets recalled, e.g., the one that has been
   * lent out for the longest period of time. If all {@link HoldingItem}s have been recalled then
   * throws a {@link RecallException}.
   */
  Recall recall(User user, CatalogueEntry entry) throws RecallException;


  /**
   * The owner of a specific {@link HoldingItem} can recall it at any time.
   */
  Recall recallAsOwner(HoldingItem item) throws RecallException;



}
