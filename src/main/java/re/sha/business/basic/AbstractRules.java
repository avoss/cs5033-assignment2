package re.sha.business.basic;

import re.sha.model.Model;

/**
 * An abstract base class for implementations of business rules. This serves to ensure that every
 * implementation of business rules has access to the data model and that this is implemented in
 * a uniform way. Unfortunately, it seems that the Jersey/HK2 dependency injection  mechanism creates
 * a new model object every time it is injected into another instance. Therefore, we are relying on
 * the {@link BasicApi} class to inject the model into the business logic.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public abstract class AbstractRules {

  protected Model model = null;

  public void setModel(Model model) {
    this.model = model;
  }
}
