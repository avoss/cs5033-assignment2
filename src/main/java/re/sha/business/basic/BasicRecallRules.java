package re.sha.business.basic;

import java.util.Set;

import re.sha.business.RecallRules;
import re.sha.exceptions.RecallException;
import re.sha.exceptions.ShareException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.Recall;
import re.sha.model.User;
import re.sha.model.jpa.JpaHelper;
import re.sha.model.jpa.JpaRecall;

/**
 * Basic rules for managing recalls. A recall is not placed if there are items available. If a
 * recall is issued, it will be for the item lent out for the longest period of time. No recalls are
 * placed items lent out for less than 7 days.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public class BasicRecallRules extends AbstractRules implements RecallRules {

  private static final int DEFAULT_RETURN_PERIOD = 7;
  
  private int returnPeriod = DEFAULT_RETURN_PERIOD;

  @Override
  public Recall recall(User user, CatalogueEntry entry) throws RecallException {
    Set<HoldingItem> available = entry.getAvailableHoldingItems();
    if (available.size() > 0) {
      throw new RecallException("Items are available, no recall placed.");
    }
    HoldingItem item = this.getLongestLentNotRecalledItem(entry);
    if (item != null) {
      try {
      Recall recall = new JpaRecall(user, item);
      item.setRecall(recall);      
      LendingRecord lendingRecord = item.getLendingRecord();
      User current = lendingRecord.getBorrower();
      this.model.notifyUser(current, "Item recalled",
          "The item " + item.getAccessionNumber() + " that you have borrowed"
              + "has been recalled. Please return it within " + this.returnPeriod  + " days.");
      return recall;
      } catch(ShareException e) {
        throw new RecallException("Error getting lending record for item to be recalled.", e);
      }
    } else {
      throw new RecallException("No items available for recall.");
    }
  }

  /**
   * Returns the {@link HoldingItem} that has been lent out for the longest time and is not already
   * recalled, ignoring any HoldingItems that are available (this should be checked separately).
   */
  public HoldingItem getLongestLentNotRecalledItem(CatalogueEntry entry) {
    HoldingItem longestOut = null;
    LendingRecord longestOutRecord = null;
    for (HoldingItem item : entry.getHoldings()) {
      try {
        if (longestOut == null) {
          if (!item.isRecalled()) {
            longestOut = item;
            longestOutRecord = item.getLendingRecord();
          }
        } else {
          if (!item.isRecalled()) {
            LendingRecord itemRecord = item.getLendingRecord();
            if (longestOutRecord.getStartDate().isAfter(itemRecord.getStartDate())) {
              longestOut = item;
              longestOutRecord = itemRecord;
            }
          }
        }
      } catch (ShareException e) {
        // ignore available items
      }
    }
    return longestOut;
  }

  /**
   * This becomes necessary when an owner issues a recall but there is another recall on the
   * HoldingItem. The previous recall needs to be re-issued.
   * 
   * @see JpaHoldingItem.recall()
   */
  public Recall reissueRecall(JpaRecall recall) throws RecallException {
    HoldingItem item =
        this.getLongestLentNotRecalledItem(recall.getHoldingItem().getCatalogueEntry());
    Recall newRecall = new JpaRecall(recall.getRecaller(), item, recall.getRecallDate());
    return newRecall;
  }

  @Override
  public Recall recallAsOwner(HoldingItem item) throws RecallException {
    if (item.isAvailable()) {
      throw new RecallException("Item is available, no recall placed.");
    }
    if (item.isRecalled()) {
      // reissue an existing recall so that the owner recall overrides it
      JpaRecall existing = JpaHelper.toJpaRecall(item.getRecall());
      this.reissueRecall(existing);
    }
    Recall recall = new JpaRecall(item.getOwner(), item);
    item.setRecall(recall);
    return recall;
  }

  public void onItemReturned(HoldingItem item) {
    if (item.isRecalled()) {
      Recall recall = item.getRecall();
      this.model.notifyUser(recall.getRecaller(), "Item available",
          "Item " + item.getAccessionNumber()
              + " has been returned and is now available to borrow for you");
    }
  }
}
