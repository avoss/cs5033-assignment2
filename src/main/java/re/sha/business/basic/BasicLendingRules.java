package re.sha.business.basic;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Set;

import re.sha.business.LendingRules;
import re.sha.exceptions.ExtendLimitReachedException;
import re.sha.exceptions.NoHoldingException;
import re.sha.exceptions.ReservationException;
import re.sha.exceptions.ShareException;
import re.sha.exceptions.TooManyItemsException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.LendingRecord;
import re.sha.model.Recall;
import re.sha.model.Reservation;
import re.sha.model.User;

public class BasicLendingRules extends AbstractRules implements LendingRules {

  private static final int DEFAULT_MAX_ITEMS = 10; // by default, a user can borrow max. 10 items
  private static final int DEFAULT_LENDING_PERIOD = 30; // 30 day default lending period
  private static final int DEFAULT_MAX_EXTENSIONS = 3; // borrowing can be extended 3 times
  private static final int DEFAULT_MAX_RESERVE = 7; // recalled items are reserved for 7 days

  private int maxItems = DEFAULT_MAX_ITEMS;
  private int lendingPeriod = DEFAULT_LENDING_PERIOD;
  private int maxExtensions = DEFAULT_MAX_EXTENSIONS;
  private int maxReserve = DEFAULT_MAX_RESERVE;

  @Override
  public LendingRecord borrow(User user, CatalogueEntry entry)
      throws NoHoldingException, TooManyItemsException, ShareException {

    if (user.getLibraryRecord().size() >= this.maxItems) {
      throw new TooManyItemsException(
          "You have reached your borrowing limit of " + this.maxItems + " items.");
    }

    Set<HoldingItem> available = entry.getAvailableHoldingItems();
    if (available.isEmpty()) {
      throw new NoHoldingException("No holding items available for this catalogue entry.");
    }

    HoldingItem item = this.model.findUserRecall(entry, user);
    if (item == null) {
      item = new ArrayList<>(available).get(0); // no attempt at usage leveling here
    } else {
      item.setRecall(null);
    }
    LendingRecord lendingRecord = this.model.borrow(item, user);
    lendingRecord.setEndDate(lendingRecord.getStartDate().plusDays(this.lendingPeriod));
    return lendingRecord;
  }

  @Override
  public LendingRecord extend(LendingRecord record) throws ExtendLimitReachedException {
    if (record.getExtended() >= this.maxExtensions) {
      throw new ExtendLimitReachedException(
          "You have reached the limit of how often an item's lending period can be extended.");
    }
    record.incExtended();
    record.setEndDate(LocalDate.now().plusDays(DEFAULT_LENDING_PERIOD));
    return record;
  }

  @Override
  public void returnItem(LendingRecord record) {
    HoldingItem item = record.getHoldingItem();
    item.returnItem();
    if (item.isRecalled()) {
      Recall recall = item.getRecall();
      User recaller = recall.getRecaller();
      this.model.notifyUser(recaller, "Recalled item available",
          "The item " + item.getAccessionNumber()
              + " that you recalled is available to borrow now. It will be reserved for you for "
              + this.maxReserve + " days");
    } else {
      CatalogueEntry catalogueEntry = item.getCatalogueEntry();
      if (catalogueEntry.isReserved()) {
        try {
          Reservation reservation = this.model.fulfillFirstReservation(catalogueEntry, item);
          this.model.notifyUser(reservation.getUser(), "Reserved item available",
              "The item " + item.getAccessionNumber()
                  + " that you reserved is available to borrow now."
                  + " It will be reserved for you for " + this.maxReserve + " days.");
        } catch(ReservationException e) {
          // TODO: can we ignore this? No reason why returning an item should fail because
          // something went wrong with the Reservations?  Just log:
        }        
      }
    }
  }
}
