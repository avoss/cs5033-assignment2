package re.sha.business.basic;

import java.util.List;
import java.util.Set;

import re.sha.business.ReservationRules;
import re.sha.exceptions.ReservationException;
import re.sha.model.CatalogueEntry;
import re.sha.model.HoldingItem;
import re.sha.model.Reservation;
import re.sha.model.User;

/**
 * Basic implementation of reservation rules. There are two restrictions:
 * 
 * <ul>
 * <li>a reservation cannot be placed on a {@link CatalogueEntry} that has {@link HoldingItem}s
 * available.</li>
 * <li>A user cannot have more than one reservation for a {@link CatalogueEntry} at any point in
 * time.</li>
 * </ul>
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
public class BasicReservationRules extends AbstractRules implements ReservationRules {

  @Override
  public Reservation makeReservation(User user, CatalogueEntry entry) throws ReservationException {
    
    Set<HoldingItem> available = entry.getAvailableHoldingItems();
    if (available.size() > 0) {
      throw new ReservationException("Items are available, no reservation placed.");
    }
    
    List<Reservation> existing = entry.getReservations();
    for(Reservation reservation: existing) {
      if(reservation.getUser() == user) {
        throw new ReservationException("User already has a reservation for this catalogue entry");
      }
    }
      
    Reservation reservation = this.model.addReservation(entry, user);
    return reservation;    
  }

}
