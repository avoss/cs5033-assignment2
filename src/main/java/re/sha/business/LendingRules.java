package re.sha.business;

import re.sha.exceptions.ExtendLimitReachedException;
import re.sha.exceptions.NoHoldingException;
import re.sha.exceptions.ShareException;
import re.sha.exceptions.TooManyItemsException;
import re.sha.model.CatalogueEntry;
import re.sha.model.LendingRecord;
import re.sha.model.Model;
import re.sha.model.User;

/**
 * Defines methods for managing the lending process.
 * 
 * @author alex.voss@st-andrews.ac.uk
 *
 */
public interface LendingRules {

  /**
   * Set reference to the {@link Model} implementation.
   */
  void setModel(Model model);

  /**
   * Borrow {@link HoldingItem} of the given {@link CatalogueEntry}. Returns a {@link LendingRecord}
   * , throws a {@link NoHoldingException} if no {@link HoldingItem} is available or a
   * {@link TooManyItemsException} if the user has come up to the limit of how many items they can
   * borrow.
   */
  public LendingRecord borrow(User user, CatalogueEntry entry)
      throws NoHoldingException, TooManyItemsException, ShareException;

  /**
   * Extend the lending period. Returns the amended {@link LendingRecord} or throws an
   * {@link ExtendLimitReachedException} if the limit of how often a lending period can be extended
   * has been reached.
   */
  public LendingRecord extend(LendingRecord record) throws ExtendLimitReachedException;

  /**
   * Return a {@link HoldingItem}, bringing the lending period to an end.
   */
  public void returnItem(LendingRecord item);

}
