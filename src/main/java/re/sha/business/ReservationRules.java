package re.sha.business;

import re.sha.exceptions.ReservationException;
import re.sha.model.CatalogueEntry;
import re.sha.model.Model;
import re.sha.model.Reservation;
import re.sha.model.User;

/**
 * Defines methods for managing {@link Reservation}s.
 * 
 * @author alex.voss@st-andrews.ac.uk
 *
 */
public interface ReservationRules {
  
  /**
   * Set reference to the {@link Model} implementation.
   */
  void setModel(Model model);
 
  /**
   * Places a {@link Reservation} on the given {@link CatalogueEntry}. The next {@link HoldingItem}
   * becoming available will be lent out to the user with the oldest Reservation.
   * @throws ReservationException 
   */
  public Reservation makeReservation(User user, CatalogueEntry entry) throws ReservationException;

}
