package re.sha.rest

import com.google.gson.JsonObject
import javax.ws.rs.client.Invocation
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response
import javax.ws.rs.core.MediaType;
import re.sha.model.jpa.JpaTestModelFactory

class ApiHoldingItemSpec extends ApiSpec {

  /**
   * Creates a {@link CatalogueEntry} and a {@link User} first, so it can then create a 
   * {@link HoldingItem} for the former with the latter as the owner.
   */
  void setupSpec() {
    JpaTestModelFactory.rollbackOnly = false
    this.entryId = this.createCatalogueEntry()
    this.userId = this.createUser()
    this.holdingItemId = this.createHoldingItem(this.entryId, this.userId)
    JpaTestModelFactory.rollbackOnly = true
  }
  
  void "getting holding item returns json"() {
    setup:
    WebTarget getTarget = target.path("item").path(this.holdingItemId)
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.get();
    String data = response.readEntity(String.class)
    JsonObject json = gson.fromJson(data, JsonObject.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    json.has("id")
    json.has("catalogueEntry")
    json.has("user")
  }
  
  void "lookup of non-existing holding item should give 404"() {
    setup:
    WebTarget getTarget = target.path("item").path("9999")
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.get();
    String data = response.readEntity(String.class)
    then:
    response.status == 404
  }
}
