package re.sha.rest

import com.google.gson.Gson;
import com.google.gson.JsonElement
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;

import javax.ws.rs.client.Invocation
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response
import javax.ws.rs.core.MediaType;
import javax.ws.rs.client.Entity

import spock.lang.Shared;
import re.sha.model.jpa.JpaTestModelFactory

class ApiCatalogueEntrySpec extends ApiSpec {

  /**
   * Creates a {@link CatalogueEntry}, a {@link User} and a {@link HoldingItem} based on them.
   */
  void setupSpec() {
    JpaTestModelFactory.rollbackOnly = false
    this.entryId = this.createCatalogueEntry()
    this.userId = this.createUser()  
    this.holdingItemId = this.createHoldingItem(this.entryId, this.userId)    
    JpaTestModelFactory.rollbackOnly = true
  }  

  void "creating a new catalogue entry should return json representation"() {
    setup:
    WebTarget createTarget = target.path("entry").path("isbn-13").path("978-1292096131")
    Invocation.Builder invocationBuilder = createTarget.request()
    when:
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    String data = response.readEntity(String.class)
    JsonObject json = gson.fromJson(data, JsonObject.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    json.has("identifier")
    json.has("identifierType")
  }
  
  void "lookup of non-existing catalogue entry should give 404"() {
    setup:
    WebTarget createTarget = target.path("entry").path("9999")
    Invocation.Builder invocationBuilder = createTarget.request()
    when:
    Response response = invocationBuilder.get();
    String data = response.readEntity(String.class)
    then:
    response.status == 404    
  }

  void "creating a holding item should return a json representation"() {
    setup:
    WebTarget createTarget = target.path("entry").path(this.entryId).path("newHoldingItem/user").path(this.userId)
    Invocation.Builder invocationBuilder = createTarget.request()
    when:
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    String data = response.readEntity(String.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    when:
    JsonObject json = gson.fromJson(data, JsonObject.class)
    then:
    json.has("user")
    json.has("catalogueEntry")
  }
  
  void "getting holdings returns a json array"() {
    setup:   
    WebTarget createTarget = target.path("entry").path(this.entryId).path("holdings")
    Invocation.Builder invocationBuilder = createTarget.request()
    when:
    Response response = invocationBuilder.get()
    String data = response.readEntity(String.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    when:
    JsonArray json = gson.fromJson(data, JsonArray.class)
    then:
    json.size() == 1
  }
    
  void "borrowing an available holding item returns lending record"() {
    setup:
    WebTarget getTarget = target.path("entry").path(this.entryId).path("borrow/user").path(this.userId)
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.get();
    String data = response.readEntity(String.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    when:
    JsonObject json = gson.fromJson(data, JsonObject.class)
    then:    
    json.has("borrower")
    json.has("holdingItem")
    json.has("startDate")
    json.has("endDate")
    json.has("extended")
  }
  
  void "creating a reservation when items available should return status 500"() {
    setup:
    WebTarget createTarget = target.path("entry").path(this.entryId).path("reservation/user").path(this.userId)
    Invocation.Builder invocationBuilder = createTarget.request()
    when:
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    String data = response.readEntity(String.class)
    then:
    response.status == 500    
  }

  void "creating a reservation when no items available should return json representation"() {
    when:
    WebTarget getTarget = target.path("entry").path(this.entryId).path("borrow/user").path(this.userId)
    Invocation.Builder invocationBuilder = getTarget.request() 
    Response response = invocationBuilder.get();
    then:
    response.status == 200    
    when:
    WebTarget createTarget = target.path("entry").path(this.entryId).path("reservation/user").path(this.userId)
    invocationBuilder = createTarget.request()
    response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    String data = response.readEntity(String.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    when:
    JsonObject json = gson.fromJson(data, JsonObject.class)
    then:
    json.has("user")
    json.has("catalogueEntry")
  }
}
