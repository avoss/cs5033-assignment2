package re.sha.rest

import java.net.URI;

import javax.ws.rs.client.Invocation
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response
import javax.ws.rs.core.MediaType;
import javax.ws.rs.client.Entity

import com.google.gson.Gson
import com.google.gson.JsonElement

import spock.lang.Shared

import re.sha.model.jpa.JpaTestModelFactory

class ApiUserSpec extends ApiSpec {  
  
  void setupSpec() {
    JpaTestModelFactory.rollbackOnly = false
    this.userId = this.createUser()    
    JpaTestModelFactory.rollbackOnly = true
  }
  
  void "creating a new user should return json representation"() {
    setup:    
    WebTarget createTarget = target.path("user").path("Dr").path("Johnny").path("Doe").path("j2@doe.com")
    Invocation.Builder invocationBuilder = createTarget.request()
    when:
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))  
    String data = response.readEntity(String.class)
    JsonElement json = gson.fromJson(data, JsonElement.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    json.isJsonObject() 
  }
  
  void "getting list of users find the test user"() {
    setup:
    WebTarget getTarget = target.path("user")
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.get()
    String data = response.readEntity(String.class)
    JsonElement json = gson.fromJson(data, JsonElement.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    json.isJsonArray()
    json.size() == 1
  }
  
  void "getting the test user should return json object with id == 1"() {
    setup:
    WebTarget getTarget = target.path("user").path("1")
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.get()
    String data = response.readEntity(String.class)
    JsonElement json = gson.fromJson(data, JsonElement.class)
    then:
    response.status == 200
    response.getMediaType() == MediaType.APPLICATION_JSON_TYPE
    json.isJsonObject()
    json.get("id").getAsInt() == 1
  }
  
  void "deleting the test user should result in empty list of users"() {
    setup:
    WebTarget getTarget = target.path("user").path("1")
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.delete()
    String data = response.readEntity(String.class)
    JsonElement json = gson.fromJson(data, JsonElement.class)
    then:
    response.status == 200   
  }
  
  void "looking for a non-existing user will return 404 (not found)"() {
    setup:
    WebTarget getTarget = target.path("user").path("9999")
    Invocation.Builder invocationBuilder = getTarget.request()
    when:
    Response response = invocationBuilder.get()
    then:
    response.status == 404
  }
  
}
