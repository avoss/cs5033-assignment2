package re.sha.rest

import java.io.IOException;
import java.net.URI

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Invocation
import javax.ws.rs.client.WebTarget
import javax.ws.rs.client.Entity
import javax.ws.rs.core.Response

import org.glassfish.grizzly.GrizzlyFuture
import org.glassfish.grizzly.http.server.HttpServer
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory
import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.server.ServerProperties;

import com.google.gson.Gson;
import com.google.gson.JsonObject

import bitronix.tm.BitronixTransactionManager;
import bitronix.tm.TransactionManagerServices
import re.sha.business.LendingRules;
import re.sha.business.RecallRules;
import re.sha.business.ReservationRules;
import re.sha.business.basic.BasicLendingRules;
import re.sha.business.basic.BasicRecallRules;
import re.sha.business.basic.BasicReservationRules;
import re.sha.model.Model
import re.sha.model.jpa.JpaModelFactory
import re.sha.model.jpa.JpaTestModelFactory

import org.junit.Ignore

import spock.lang.Shared
import spock.lang.Specification

/**
 * Base class for running end-to-end integration tests that exercise the RESTful API. To set up
 * a number of objects for your tests in {@link #setupSpec()}, set <tt>JpaTestModelFactory.rollbackOnly = false</tt>,
 * then create your objects and set <tt>JpaTestModelFactory.rollbackOnly = true</tt>. This will
 * ensure that the changes are persisted to the database while any further changes during the tests
 * themselves are rolled back.
 */
@Ignore
class ApiSpec extends Specification {

  public static final URI BASE_URI = URI.create("http://127.0.0.1:9998/");
  private HttpServer httpServer = null;

  @Shared
  protected WebTarget target = null;

  @Shared
  protected Gson gson = new Gson()

  @Shared
  protected String entryId = ""

  @Shared
  protected String userId = ""

  @Shared
  protected String holdingItemId = ""

  void setupSpec() {
    this.httpServer  = this.createServer();
    this.httpServer.start();

    Client client = ClientBuilder.newClient()
    target = client.target(BASE_URI)
  }

  void cleanupSpec() {
    GrizzlyFuture<HttpServer> shutdown = this.httpServer.shutdown()
    while(!shutdown.isDone()) {
      // wait for the shutdown to be completed before continuing
      try {
        Thread.sleep(200)
      } catch(InterruptedException e) {
        // ignore
      }
    }
    // The transactionmanager does not get cleaned up properly in JpaModelFactory.dispose, because
    // this method does not see to get called on shutdown of Grizzly, so we are cleaning this up here.
    BitronixTransactionManager tx =
        TransactionManagerServices.getTransactionManager();
    tx.shutdown()
  }

  /**
   * Create a Grizzly server and register the classes that make up this application.
   */
  private HttpServer createServer() throws IOException {
    JpaModelFactory.setDsPropsFilename("config/testdatasources.properties")
    final ResourceConfig rc = new ResourceConfig()
    rc.packages("re.sha.rest")
    rc.property(ServerProperties.TRACING, "ALL");
    rc.register(new AbstractBinder() {
          @Override
          protected void configure() {
            bindFactory(JpaTestModelFactory.class).to(Model.class)
            bind(BasicLendingRules.class).to(LendingRules.class);
            bind(BasicRecallRules.class).to(RecallRules.class);
            bind(BasicReservationRules.class).to(ReservationRules.class);
          }
        });
    return GrizzlyHttpServerFactory.createHttpServer(this.BASE_URI, rc)
  }

  /**
   * Create a {@link CatalogueEntry} for testing.
   */
  String createCatalogueEntry() {    
    WebTarget createTarget = target.path("entry").path("isbn-13").path("978-1292096131")
    Invocation.Builder invocationBuilder = createTarget.request()
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    if(response.status != 200) {
      throw new RuntimeException("Error creating test catalogue entry.")
    }
    JsonObject json = gson.fromJson(response.readEntity(String.class), JsonObject.class)    
    return json.get("id").getAsString()
  }

  /**
   * Create a {@User} for testing.
   */
  String createUser() {
    WebTarget createTarget = target.path("user").path("Dr").path("Johnny").path("Doe").path("j@doe.com")
    Invocation.Builder invocationBuilder = createTarget.request()
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    if(response.status != 200) {
      throw new RuntimeException("Error creating test user.")
    }
    JsonObject json = gson.fromJson(response.readEntity(String.class), JsonObject.class)    
    return json.get("id").getAsString()
  }

  /**
   * Create a {@link HoldingItem} for the {@link CatalogueEntry} and {@link User} already
   * created.
   */
  String createHoldingItem(String entryId, String userId) {    
    WebTarget createTarget = target.path("entry").path(entryId).path("newHoldingItem/user").path(userId)
    Invocation.Builder invocationBuilder = createTarget.request()
    Response response = invocationBuilder.put(Entity.json(gson.toJson("{}")))
    if(response.status != 200) {
      throw new RuntimeException("Unable to create holding item: " + response.readEntity(String.class))
    }
    JsonObject json = this.gson.fromJson(response.readEntity(String.class), JsonObject.class)
    return json.get("id").getAsString()
  }
}
