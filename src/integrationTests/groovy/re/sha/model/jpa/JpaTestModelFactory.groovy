package re.sha.model.jpa

import re.sha.model.Model;

class JpaTestModelFactory extends JpaModelFactory {
  
  public static rollbackOnly = false
  
  @Override
  public Model provide() {
    def model = super.provide();
    if(rollbackOnly) {
      model.setRollbackOnly()
    }
    return model 
  }
  
}
