package re.sha.model.jpa

import javax.mail.internet.InternetAddress
import re.sha.model.User;

class JpaLendingLogicSpec extends JpaSpec {
  
  JpaCatalogueEntry testCatalogueEntry = null
  JpaUser testUser = null;

  JpaCatalogueEntry makeTestCatalogueEntry() {
    this.testCatalogueEntry = new JpaCatalogueEntry("abc","someType")
    this.em.persist(testCatalogueEntry)
    return testCatalogueEntry
  }

  User makeTestUser() {
    User testUser = new JpaUser()
    testUser.setTitle("Dr")
    testUser.setName("John")
    testUser.setSurname("Doe")
    testUser.setEMail(new InternetAddress("john@doe.com"))
    this.em.persist(testUser)
    return testUser
  }
  
  void setup() {
    //super.setup() - this is invoked by the framework, so no need to call explicitly
    this.testCatalogueEntry = this.makeTestCatalogueEntry()
    this.testUser = this.makeTestUser()
  }
  
  void "when adding a HoldingItem for a CatalogueEntry, association is navigable in both directions"() {
    when:
    JpaHoldingItem testHoldingItem = this.testCatalogueEntry.createHoldingItem(this.testUser)
    this.em.persist(testHoldingItem)
    long id = testCatalogueEntry.getId()
    this.em.flush()
    this.em.clear() // we want *new* Java objects from here on
    this.testCatalogueEntry = this.em.find(JpaCatalogueEntry.class, id);
    then:
    id == this.testCatalogueEntry.getId()
    for(JpaHoldingItem i: this.testCatalogueEntry.getHoldings()) {
      i.getCatalogueEntry() == this.testCatalogueEntry
    }
  }
  
  void "when setting owner for a HoldingItem, association is navigable in both directions"() {
    when:
    JpaHoldingItem testHoldingItem = this.testCatalogueEntry.createHoldingItem(this.testUser)
    this.em.persist(testHoldingItem)
    long id = testUser.getId()
    this.em.flush()
    this.em.clear() // we want *new* Java objects from here on
    this.testUser = this.em.find(JpaUser.class, id);
    then:    
    for(JpaHoldingItem i: this.testUser.getHoldings()) {
      i.getOwner() == this.testUser      
    }
  }
  
  void "when borrowing a HoldingItem, it should become unavailable"() {
    when:
    JpaUser testBorrower = new JpaUser("Dr", "Joe", "Smith", "joe@smith.com");
    JpaHoldingItem testHoldingItem = this.testCatalogueEntry.createHoldingItem(this.testUser)
    testHoldingItem.borrow(testBorrower)
    then:
    testHoldingItem.isAvailable() == false
  }
}
