package re.sha.model.jpa

import javax.mail.internet.InternetAddress
import re.sha.model.jpa.JpaHoldingItem
import re.sha.model.CatalogueEntry
import re.sha.model.HoldingItem
import re.sha.model.LendingRecord
import re.sha.model.User;
import spock.lang.Specification
import java.time.LocalDate

class JpaCatalogueEntrySpec extends JpaSpec {

  JpaCatalogueEntry testCatalogueEntry = null
  JpaUser testUser = null
  JpaUser testUser2 = null

  JpaCatalogueEntry makeTestCatalogueEntry() {
    this.testCatalogueEntry = new JpaCatalogueEntry("abc","someType")
    this.em.persist(testCatalogueEntry)
    return testCatalogueEntry
  }
  
  User makeTestUser() {
    User testUser = new JpaUser()
    testUser.setTitle("Mr")
    testUser.setName("John")
    testUser.setSurname("Doe")
    testUser.setEMail(new InternetAddress("john@doe.com"))
    this.em.persist(testUser)
    return testUser
  }
  
  User makeTestUser2() {
    User testUser = new JpaUser()
    testUser.setTitle("Dr")
    testUser.setName("Jane")
    testUser.setSurname("Doe")
    testUser.setEMail(new InternetAddress("jane@doe.com"))
    this.em.persist(testUser)
    return testUser
  }

  void setup() {
    //super.setup() - this is invoked by the framework, so no need to call explicitly
    this.testCatalogueEntry = this.makeTestCatalogueEntry()
    this.testUser = this.makeTestUser()
    this.testUser2 = this.makeTestUser2()
  }

  void "when adding a field that already exists, old value is overwritten"() {
    when:
    this.testCatalogueEntry.addField("someKey","someValue")
    this.em.persist(this.testCatalogueEntry)
    this.em.flush()
    this.testCatalogueEntry.addField("someKey","newValue")
    this.em.persist(this.testCatalogueEntry)
    long id = this.testCatalogueEntry.getId()
    this.em.flush() 
    this.em.clear() // make sure the changed do go though the database layer
    this.testCatalogueEntry = this.em.find(JpaCatalogueEntry.class, id)    
    then:
    this.testCatalogueEntry.getValue("someKey").equals("newValue")
  }
  
  void "when creating a new HoldingItem, it is in user and catalogue entry holdings"() {
    when:
    HoldingItem item = this.testCatalogueEntry.createHoldingItem(this.testUser);
    this.em.persist(item)
    long id = item.getId()
    this.em.flush()
    this.em.clear() // make sure the changed do go though the database layer
    item = this.em.find(JpaHoldingItem.class, id)
    then:
    item.getCatalogueEntry().getHoldings().contains(item)
    item.getOwner().getHoldings().contains(item)
  }
  
  void "when creating HoldingItems, only those that are available get listed by getAvailableHoldingItems"() {
    when:
    long id = this.testCatalogueEntry.getId()
    HoldingItem available = this.testCatalogueEntry.createHoldingItem(this.testUser)
    HoldingItem unavailable = this.testCatalogueEntry.createHoldingItem(this.testUser)
    LendingRecord lr = unavailable.borrow(this.testUser2) 
    lr.setEndDate(LocalDate.now())   
    this.em.persist(available)
    this.em.persist(unavailable)
    this.em.persist(lr)
    this.em.flush()
    this.em.clear()
    CatalogueEntry ce = this.em.find(JpaCatalogueEntry.class, id)
    then:
    ce.getAvailableHoldingItems().size() == 1
  }
}
