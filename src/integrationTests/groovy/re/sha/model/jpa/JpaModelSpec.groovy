package re.sha.model.jpa

import javax.mail.internet.InternetAddress
import re.sha.model.CatalogueEntry
import re.sha.model.HoldingItem
import re.sha.model.Model
import re.sha.model.Reservation
import re.sha.model.User;

import javax.persistence.EntityManager
import javax.transaction.TransactionManager

/**
 * Tests for the named queries and other data retrieval methods in the {@link JpaModel} implementation.
 * 
 * @author alex.voss@st-andrews.ac.uk
 */
class JpaModelSpec extends JpaSpec {
  
  Model model = null; 
  User testUser = null  
  
  User makeTestUser() {
    User testUser = new JpaUser()
    testUser.setTitle("Dr")
    testUser.setName("John")
    testUser.setSurname("Doe")
    testUser.setEMail(new InternetAddress("john@doe.com"))
    this.em.persist(testUser)
    return testUser
  }
  
  void setup() {
    this.model = new JpaModel(Mock(EntityManager), Mock(TransactionManager)) 
    //super.setup() - this is invoked by the framework, so no need to call explicitly
    this.testUser = this.makeTestUser()
  }

  void "when fulfillFirstReservation() is called, moves reservation from catalogueentry to holdingitem"() {
    setup:   
    CatalogueEntry entry = new JpaCatalogueEntry("someId", "someType")
    HoldingItem item = entry.createHoldingItem(this.testUser)
    Reservation reservation = this.model.addReservation(entry, this.testUser)
    when:
    Reservation fulfilled = this.model.fulfillFirstReservation(entry, item)
    then:
    fulfilled == reservation
    reservation.getCatalogueEntry() == null
    reservation.getHoldingItem() == item
  }
}
