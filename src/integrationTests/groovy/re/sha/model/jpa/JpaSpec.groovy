package re.sha.model.jpa

import org.junit.Ignore
import spock.lang.Shared
import spock.lang.Specification
import javax.naming.Context
import javax.naming.InitialContext
import javax.persistence.Persistence
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.transaction.TransactionManager
import javax.transaction.Status
import bitronix.tm.Configuration
import bitronix.tm.TransactionManagerServices

/**
 * Enables testing of ORM mappings by providing an JPA {@link EntityManager} that is configured to use an
 * in-memory database for testing purposes.
 *
 * The way that JPA and JTA are wired up using JNDI (assuming the code will run in a managed container
 * environment that provides the necessary infrastructure) makes unit testing more difficult to configure.
 * I used information from these blog posts to solve the problem:
 * 
 * https://blogs.oracle.com/randystuph/entry/injecting_jndi_datasources_for_junit
 * 
 * The JNDI implementation used is from the Glassfish project, see Gradle build file.
 */
@Ignore
class JpaSpec extends Specification {

  @Shared
  protected EntityManagerFactory emf = null
  @Shared
  protected TransactionManager tx = null

  protected EntityManager em = null

  /**
   * Create the transaction manager and entity manager - these are available via protected instance 
   * variables in this class, which can be used by subclasses. 
   */
  void setupSpec() {
    this.tx = this.setupTx(this.getDatasourcePropsFilename());
    this.tx.begin() // need to create EMFactory in transaction so it can modify schema
    try {
      this.emf = Persistence.createEntityManagerFactory("ShaRePU");
    } catch(Exception e) {
      this.tx.rollback()
      throw new RuntimeException(e)
    }
    this.tx.commit()
  }

  /**
   * Sets up a Jndi context that the <a href="https://github.com/bitronix/btm">Bitronix</a> JTA 
   * transaction manager can use to register the {@link DataSource}s. 
   */
  void setupJndi() {
    System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "bitronix.tm.jndi.BitronixInitialContextFactory")
    InitialContext ic = new InitialContext()
  }

  /**
   * Sets up the Bitronix transaction manager using the given datasource properties file.
   */
  TransactionManager setupTx(String dsPropsFilename) {
    Configuration conf = TransactionManagerServices.getConfiguration();
    conf.setResourceConfigurationFilename(dsPropsFilename)
    conf.setSkipCorruptedLogs(true);
    this.tx = TransactionManagerServices.getTransactionManager()
  }

  /**
   * Allows a build script to use different datasource properties to facilitate testing with 
   * different kinds of database implementations. To do this is has to set the system property
   * DATASOURCE_PROPERTIES. If this is not set then a default value ("config/testdatasources.properties") 
   * is returned.
   */
  String getDatasourcePropsFilename() {
    Properties props = System.getProperties()
    String dsProperties = null
    if(props.hasProperty("DATASOURCE_PROPERTIES")) {
      return System.getProperties().getProperty("DATASOURCE_PROPERTIES")
    }
    return "config/testdatasources.properties";
  }

  /**
   * Setup method creates a new {@link EntityManager} for each test and begins a transactions, which is rolled back
   * by cleanup(), so that a test cannot affect subsequent ones. Override if more functionality is required.
   */
  void setup() {
    this.em = this.emf.createEntityManager()
    this.tx.begin()
  }

  /**
   * Rolls back the transaction for the test and closes the {@link EntityManager} opened in setup().
   */
  void cleanup() {
    this.tx.rollback()
    this.em.close()
  }

  /**
   * Closes the EntityManagerFactory.
   */
  void cleanupSpec() {
    // roll back any open transactions
    if(this.tx.getStatus() != Status.STATUS_NO_TRANSACTION) {
      this.tx.rollback()
    }
    if(this.emf != null) {
      this.emf.close()
    }
    this.tx.shutdown()
  }

}