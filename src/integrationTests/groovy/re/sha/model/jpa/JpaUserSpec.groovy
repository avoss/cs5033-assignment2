package re.sha.model.jpa

import re.sha.model.*
import bitronix.tm.internal.BitronixRollbackException
import javax.mail.internet.InternetAddress
import javax.validation.ValidationException
import javax.validation.ConstraintViolationException

class JpaUserSpec extends JpaSpec {

  User testUser = null

  User makeTestUser() {
    User testUser = new JpaUser()
    testUser.setTitle("Dr")
    testUser.setName("John")
    testUser.setSurname("Doe")
    testUser.setEMail(new InternetAddress("john@doe.com"))
    this.em.persist(testUser)
    return testUser
  }

  void setup() {
    //super.setup() - this is invoked by the framework, so no need to call explicitly
    this.testUser = this.makeTestUser()
  }

  void "when creating new user, length of user list should be one"() {
    def users = null
    when:
    // user is created in setup()
    users = em.createQuery("select u from JpaUser u").getResultList()
    then:
    users.size() == 1
  }
  
  void "when creating a new user, they have no holdingitems"() {
    expect:
    testUser.getHoldings().size() == 0
  }
  
  void "when creating a new user, they have no lending record"() {
    expect:
    testUser.getLibraryRecord().size() == 0
  }

  void "when creating a new User with no surname, an exception is thrown"() {
    when:
    User testUser = new JpaUser()
    testUser.setTitle("Dr")
    testUser.setName("John")
    testUser.setEMail(new InternetAddress("john@doe.com"))
    this.em.persist(testUser)
    this.em.flush()
    then:
    thrown ConstraintViolationException
  }

  void "email uniqueness must be enforced"() {
    when:
    User anotherUser = new JpaUser()
    testUser.setTitle("Dr")
    testUser.setName("John")
    testUser.setSurname("Doe Jr.")
    testUser.setEMail(new InternetAddress("john@doe.com"))
    this.em.persist(anotherUser)
    this.em.flush()
    then:
    thrown ConstraintViolationException
  }
}