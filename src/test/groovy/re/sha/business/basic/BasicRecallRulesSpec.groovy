package re.sha.business.basic

import java.time.LocalDate
import java.time.temporal.ChronoUnit
import javax.persistence.EntityManager
import javax.transaction.TransactionManager

import re.sha.model.CatalogueEntry
import re.sha.model.HoldingItem
import re.sha.model.LendingRecord
import re.sha.model.Model
import re.sha.model.Recall
import re.sha.model.User
import re.sha.model.jpa.JpaCatalogueEntry
import re.sha.model.jpa.JpaHoldingItem
import re.sha.model.jpa.JpaModel
import re.sha.model.jpa.JpaUser
import re.sha.exceptions.RecallException
import spock.lang.Specification

/**
 * Unit tests for the {@link BasicRecallRules}.
 */
class BasicRecallRulesSpec extends Specification {
  
  BasicRecallRules rules = null
  Model model = null
  JpaUser mockUser = Mock()  
  JpaHoldingItem mockHoldingItem = Mock()
  
  void setup() {
    this.rules = new BasicRecallRules()
    this.model = new JpaModel(Mock(EntityManager), Mock(TransactionManager))
    this.rules.setModel(this.model)
  }
  
  void "when there are no holdings to satsify a recall resuest, RecallException is thrown"() {
    setup:
    CatalogueEntry mockCatalogueEntry = Mock()
    Set<HoldingItem> noholdings = new HashSet<HoldingItem>()
    mockCatalogueEntry.getAvailableHoldingItems() >> noholdings     
    mockCatalogueEntry.getHoldings() >> noholdings    
    when:
    Recall recall = this.rules.recall(mockUser, mockCatalogueEntry);
    then:
    thrown RecallException
  }
  
  void "when there are available HoldingItems, RecallException is thrown"() {
    setup:
    CatalogueEntry mockCatalogueEntry = Mock()
    Set<HoldingItem> mockSet = Mock()
    mockCatalogueEntry.getAvailableHoldingItems() >> mockSet
    mockSet.size() >> 1
    when:
    Recall recall = this.rules.recall(mockUser, mockCatalogueEntry);
    then:
    thrown RecallException    
  }
  
  void "when recall is made, item longest out of the library is chosen"() {
    setup: // good job we can use the real classes here, mocking this would be a pain!   
    JpaUser user = new JpaUser(); 
    CatalogueEntry catalogueEntry = new JpaCatalogueEntry("someId","someIdType")    
    HoldingItem item1 = catalogueEntry.createHoldingItem(user)
    LendingRecord lr1 = item1.borrow(user); 
    HoldingItem item2 = catalogueEntry.createHoldingItem(user)
    LendingRecord lr2 = item2.borrow(user);
    lr2.setStartDate(LocalDate.now().minus(1,ChronoUnit.DAYS))
    HoldingItem item3 = catalogueEntry.createHoldingItem(user)
    LendingRecord lr3 = item3.borrow(user);
    when:
    Recall recall = this.rules.recall(this.mockUser, catalogueEntry);
    then:
    recall.getHoldingItem() == item2
  }
  
  void "when recall is issued, current borrower is notified"() {
    setup:
    JpaUser user = new JpaUser();
    JpaUser user2 = new JpaUser();
    CatalogueEntry catalogueEntry = new JpaCatalogueEntry("someId","someIdType")
    HoldingItem item = catalogueEntry.createHoldingItem(user)
    item.borrow(user)
    when:
    this.rules.recall(user2, catalogueEntry)
    then:
    user.getNotifications().size() > 0
  }
  
  void "when a recall is satisfied, user is notified"() {
    setup:
    JpaUser user = new JpaUser();
    JpaUser user2 = new JpaUser();
    CatalogueEntry catalogueEntry = new JpaCatalogueEntry("someId","someIdType")
    HoldingItem item = catalogueEntry.createHoldingItem(user)
    item.borrow(user)
    this.rules.recall(user2, catalogueEntry)    
    when:
    this.rules.onItemReturned(item)
    then:
    user2.getNotifications().size() > 0
  }
}
